<?php

namespace App\Http\Controllers;

use App\Exports\KunjunganAsingExport;
use App\Exports\KunjunganOwExport;
use App\KunjunganOw;
use App\Ow;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

use RealRashid\SweetAlert\Facades\Alert;

class KunOwController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        /*$bulan = $request->get('bulan').'-01';
        $bulan =date('Y-m-d',strtotime($bulan));
        $alertmont = date('F',strtotime($bulan));
        $search = KunjunganOw::where('bulan', $bulan)->where('objek_wisata_id', $request->get('id'));
        if ($search->count() > 0) {
            Alert::error('Gagal', ' Bulan '.$alertmont.' Telah ada');
            return redirect()->back();
        } else {
            $ow = new KunjunganOw();
            $ow->bulan = $bulan;
            $ow->jumlah = $request->get('jumlah');
            $ow->jumlah_asing = $request->get('jumlah_asing'); 
            $ow->objek_wisata_id = $request->get('id');
            $ow->users_id = Auth::user()->id;
            $ow->save();
            Alert::success('Sukses', 'Kunjungan Berhasil ditambah');
            return redirect()->back();
            
        }*/

        $curyear = date('2021');
        // $curyear = date('Y');
        $data =[
            ['bulan' => $curyear.'-01-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-02-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-03-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-04-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-05-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-06-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-07-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-08-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-09-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-10-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-11-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-12-01', 'jumlah' => 0, 'jumlah_asing' => 0, 'objek_wisata_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
        ];

        KunjunganOw::insert($data);

        $user = Ow::find($request->get('id'));
        $user->generated_at = $curyear;
        $user->save();

        Alert::success('Sukses', 'Generate');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$curyear = date('Y'); 
        $curyear = date('2021'); 
        $ow = Ow::find($id);
        $datas = KunjunganOw::where('objek_wisata_id', $id)->orderBy('bulan', 'asc')->get();

        $KunThisYear = KunjunganOw::where('objek_wisata_id', $id)->whereYear('bulan', $curyear)->sum('jumlah');
        $avgPerMonth = KunjunganOw::where('objek_wisata_id', $id)->whereYear('bulan', $curyear)->pluck('jumlah')->avg();
        $avgPerMonth = number_format($avgPerMonth);

        //$gkun = KunjunganOw::select(DB::raw("DATE_FORMAT(bulan, '%M') as bulan"))->where('objek_wisata_id', $id)->orderBy(DB::raw("DATE_FORMAT(bulan, '%m')", 'asc' ))->get();
        //$total = KunjunganOw::select(DB::raw('jumlah as jumlah'))->where('objek_wisata_id', $id)->orderBy(DB::raw("DATE_FORMAT(bulan, '%m')", 'asc' ))->get();

        $record = KunjunganOw::select([
            DB::raw(DB::raw("DATE_FORMAT(bulan, '%M') as bulan")),
            DB::raw(DB::raw('jumlah as jumlah')),
            DB::raw(DB::raw('jumlah_asing as asing'))
        ])->where('objek_wisata_id', $id)->where(DB::raw("DATE_FORMAT(bulan, '%Y')", $curyear ), $curyear)->orderBy(DB::raw("DATE_FORMAT(bulan, '%m')", 'asc' ))->get();

        $data = [];
        foreach ($record as $row) { 
            # code...
            $data['label'][] = $row->bulan;
            $data['data1'][] = (int) $row->jumlah;
            $data['data2'][] = (int) $row->asing;
        }
        $data['chart_data'] = json_encode($data);
        return view('ow.kunjungan.index')->with($data)->with('avgPerMonth', $avgPerMonth)->with('KunThisYear', $KunThisYear )->with('ow', $ow)->with('datas', $datas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->get('id');
        $kunOw = KunjunganOw::find($id);
        $kunOw->jumlah = $request->get('jumlah');
        $kunOw->jumlah_asing = $request->get('jumlah_asing');
        $kunOw->save();
        Alert::success('Sukses', 'Updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function exportOw($tahun)
    {
        /*$datas = Ow::with(['kunjungan' => function($query) {
            $query->whereYear('bulan', date('Y'));
        }])->get();
        return view('tested.index', compact('datas'));
        */

        $namafile = 'KUNJUNGAN_TAHUN_'.$tahun.''.'.xlsx';
        return Excel::download(new KunjunganOwExport($tahun), $namafile);
    }

    public function exportOwAsing($tahun)
    {

        $namafile = 'KUNJUNGAN_ASING_TAHUN_'.$tahun.''.'.xlsx';
        return Excel::download(new KunjunganAsingExport($tahun), $namafile);
    }
}
