<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkrafEkraf extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'ekraf';
    protected $fillable = ['ekraf_nama', 'ekraf_alamat', 'ekraf_kecamatan', 'ekraf_desa', 'ekraf_kepemilikan', 'ekraf_whatsapp', 'ekraf_tahun_berdiri', 'ekraf_sub_sektor', 'ekraf_haki', 'ekraf_keterangan', 'users_id','kurasi'];
}
