<?php

namespace App\Http\Controllers;

use App\KunjunganRhu;
use App\Rhu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class RhuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $curyear = date('2021'); 
        //$curyear = date('Y'); 
        $datas = Rhu::get();
        $record = KunjunganRhu::select([
            DB::raw(DB::raw("DATE_FORMAT(bulan, '%M') as bulan")),
            DB::raw(DB::raw('SUM(jumlah) as jumlah')),
            DB::raw(DB::raw('SUM(jumlah_asing) as asing'))
        ])->where(DB::raw("DATE_FORMAT(bulan, '%Y')" ), $curyear)->groupBy('bulan')->get();

        $data = [];
        foreach ($record as $row) {
            # code...
            $data['label'][] = $row->bulan;
            $data['data1'][] = (int) $row->jumlah;
            $data['data2'][] = (int) $row->asing;
        }
        $data['chart_data'] = json_encode($data);
        
        $kunjunganTahun = KunjunganRhu::selectRaw('year(bulan) year, count(*) data')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();

        return view('rhu.index')->with($data)->with('datas', $datas)->with('kunjunganTahun', $kunjunganTahun);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rhu = new Rhu();
        $rhu->nama = $request->get('nama');
        $rhu->tdup = $request->get('tdup');
        $rhu->chse = $request->get('chse');
        $rhu->peduli_lindungi = $request->get('peduli');
        $rhu->whatsapp = $request->get('whatsapp'); 
        $rhu->jenis = $request->get('jenis'); 
        $rhu->save();
        Alert::success('Sukses', 'Berhasil Ditambah');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->get('id');
        $rhu = Rhu::find($id);
        $rhu->nama = $request->get('nama');
        $rhu->whatsapp = $request->get('whatsapp');
        $rhu->tdup = $request->get('tdup');
        $rhu->chse = $request->get('chse');
        $rhu->peduli_lindungi = $request->get('peduli');
        if ($request->get('jenis') != 'Pilih') {
            $rhu->jenis = $request->get('jenis');
            }
        $rhu->save();
        Alert::success('Sukses', 'Berhasil di Update');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
