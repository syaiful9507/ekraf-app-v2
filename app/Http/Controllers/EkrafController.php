<?php

namespace App\Http\Controllers;

use App\EkrafEkraf;
use App\Exports\EkrafExports;
use App\Ownership;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravolt\Indonesia\Models\District;
use Laravolt\Indonesia\Models\Village;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class EkrafController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $CODE = $id;
        if ($CODE == 1) {
            $datas = EkrafEkraf::where('kurasi',1)->get();
        } else {
            $datas = EkrafEkraf::where('kurasi','!=', 1)->get();
        }
        
        
    
        $district = District::where('city_id','3201')->pluck('name','id');
        $ownerships =  Ownership::pluck('name','id');
        return view('ekraf.index')->with('datas',$datas)->with('district', $district)->with('ownerships', $ownerships)->with('code',$CODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ownerships =  Ownership::pluck('name','id');
        $district = District::where('city_id','3201')->pluck('name','id');
        return view('ekraf.create', compact(['ownerships', 'district']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $districtID = $request->get('district');
        $villageID = $request->get('village');

        $district = District::find($districtID);
        $village = Village::find($villageID);

        $nama = $request->get('nama');
        $search = EkrafEkraf::where('ekraf_nama', 'LIKE','%'.$nama.'%')->first();
        if ($search == NULL) {
            $ekraf = new EkrafEkraf();
            $ekraf->ekraf_nama = $request->get('nama');
            $ekraf->ekraf_alamat = $request->get('alamat');
            $ekraf->ekraf_kecamatan = $district->name;
            $ekraf->ekraf_desa = $village->name;
            $ekraf->ekraf_kepemilikan = $request->get('kepemilikan');
            $ekraf->ekraf_whatsapp = $request->get('nomor_telepon');
            $ekraf->ekraf_tahun_berdiri = $request->get('tahun_berdiri');
            $ekraf->ekraf_sub_sektor = $request->get('sub_sektor');
            $ekraf->ekraf_haki = $request->get('haki');
            $ekraf->ekraf_keterangan = $request->get('keterangan');
            $ekraf->save();
            Alert::success('Sukses', 'Berhasil ditambahkan ke database');
            return redirect()->route('ekraf.index');
        } else {
            Alert::error('Gagal', 'Data Telah ada ');
            return redirect()->route('ekraf.index');
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ekraf = EkrafEkraf::find($id);
        $district = District::where('city_id','3201')->pluck('name','id');
        $ownerships =  Ownership::pluck('name','id');
        return view('ekraf.show', compact(['ekraf','ownerships', 'district']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ekraf = EkrafEkraf::find($id);
        $district = District::where('city_id','3201')->pluck('name','id');
        $ownerships =  Ownership::pluck('name','id');
        return view('ekraf.edit', compact(['ekraf','ownerships', 'district']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $district = District::find($request->district);
        $village = Village::find($request->village);


        if ($request->village == NULL) {
            
            EkrafEkraf::find($id)->update([
                'ekraf_nama' => $request->get('nama'),
                'ekraf_alamat' => $request->get('alamat'),
                'ekraf_sub_sektor' => $request->get('sub_sektor'),
                'ekraf_kepemilikan' => $request->get('kepemilikan'),
                'ekraf_whatsapp' => $request->get('nomor_telepon'),
                'ekraf_tahun_berdiri' => $request->get('tahun_berdiri'),
                'ekraf_haki' => $request->get('haki'),
                'ekraf_keterangan' => $request->get('keterangan'),
            ]);

            Alert::success('Sukses', 'Data berhasil diupdate');
            return redirect()->route('ekraf.index');
            

        } else {
            EkrafEkraf::find($id)->update([
                'ekraf_nama' => $request->get('nama'),
                'ekraf_alamat' => $request->get('alamat'),
                'ekraf_sub_sektor' => $request->get('sub_sektor'),
                'ekraf_kepemilikan' => $request->get('kepemilikan'),
                'ekraf_whatsapp' => $request->get('nomor_telepon'),
                'ekraf_tahun_berdiri' => $request->get('tahun_berdiri'),
                'ekraf_haki' => $request->get('haki'),
                'ekraf_keterangan' => $request->get('keterangan'),
                'ekraf_kecamatan' => $district->name,
                'ekraf_desa' => $village->name,
            ]);
            Alert::success('Sukses', 'Data berhasil diupdate');
            return redirect()->route('ekraf.index');
        }
    }

    public function destroy($id)
    {
        
    }
    public function export($code)
    {
            $date = Carbon::now();
            $namafile = 'DATA_EKONOMI_KREATIF_'.$date.' '.'.xlsx';
            return Excel::download(new EkrafExports($code), $namafile);
        
        
        
    }


    public function kurasi($id,$code)
    {
        if ($code == 1) {
            $value = 0;
            $sukses ='Data berhasil di Restore';
        } else {
            $value = 1;
            $sukses ='Data berhasil diakurasi';
        }
        
        EkrafEkraf::find($id)->update([
        'kurasi' => $value,
        ]);
        Alert::success('Sukses', $sukses);      
        return redirect()->back();  
    }
}
