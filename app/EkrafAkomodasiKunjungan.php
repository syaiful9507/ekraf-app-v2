<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkrafAkomodasiKunjungan extends Model
{
    protected $connection = 'mysql2';
    protected $table = "akomodasi_kunjungan";
    protected $fillable = ['bulan', 'jumlah', 'users_id', 'akomodasi_id'];

}
