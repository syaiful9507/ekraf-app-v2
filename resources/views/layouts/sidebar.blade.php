<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="{{ route('home') }}"><img alt="image" width="120" height="50" src="{{ asset('assets/img/ekraf.png') }}"></a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ route('home') }}"><img alt="image"  width="30" height="30" src="{{ asset('assets/img/ekraf-medium.png') }}"></a>
      </div>
      <ul class="sidebar-menu">
        <li class="dropdown  {{ (request()->is('home*')) ? 'active' : '' }}">
          <a href="{{ route('home') }}" class="nav-link "><i class="fas fa-fire"></i><span>DASHBOARD</span></a>
        </li>
        <li class="dropdown {{ (request()->is(['ekraf_index/0'])) ? 'active' : '' }}">
            <a href="{{ route('ekraf_index',0) }}" class="nav-link"><i class="fas fa-swimming-pool"></i> <span>EKRAF</span></a>
        </li>

        <li class="dropdown {{ (request()->is(['ekraf_index/1'])) ? 'active' : '' }}">
          <a href="{{ route('ekraf_index',1) }}" class="nav-link"><i class="fas fa-swimming-pool"></i> <span>KURASI</span></a>
      </li>
        @if (Auth::user()->level == 1)
        <li class="menu-header">Access</li>
        <li class="dropdown {{ (request()->is(['user*'])) ? 'active' : '' }}">
            <a href="{{ route('user.index') }}" class="nav-link"><i class="fas fa-users"></i> <span>USER</span></a>
        </li>
        @endif
        
      </ul>

      <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
        <a href="https://docs.google.com/document/d/1Q0mENLNhf7JPvET8uqzymEq4Qt7iP_MLzJXkkqdAN4g/edit?usp=sharing" target="_blank" class="btn btn-primary btn-lg btn-block btn-icon-split">
          <i class="fas fa-rocket"></i> Documentation
        </a>
      </div>        
    </aside>
  </div>