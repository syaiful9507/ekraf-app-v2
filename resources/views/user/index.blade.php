@extends('layouts.app')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>User Akses</h1>
    </div>
    
    @error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror    
    <div class="section-body">
      <h2 class="section-title">Data User</h2>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                <button type="button" class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="far fa-edit"></i>Masukkan User</button>
                
                
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>                                 
                    <tr>
                      <th class="text-center">
                        #
                      </th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Level Akses</th>
                      <th>Action</th>
                    </tr>
                  </thead> 
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($datas as $item)
                    <tr>
                      <td class="text-center">
                        {{ $no++ }}
                      </td>
                      <td>{{ $item->name }}</td>
                      <td>{{ $item->email }}</td>
                      <td>
                        @if ($item->level === 2)
                        <div class="badge badge-danger">USER EXTERNAL</div>                             
                        @endif
                        @if ($item->level === 0)
                        <div class="badge badge-secondary">USER INTERNAL</div>
                        @endif
                        @if ($item->level === 1)
                        <div class="badge badge-primary">SUPER USER</div>
                        @endif
                      </td>
                        <td>
                            @if ($item->id == Auth::user()->id)
                            <div class="badge badge-info">Anda</div>
                            @elseif ($item->level != 1)
                            <form action="{{ route('user.destroy', $item->id) }}"   method="post">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-icon icon-left btn-danger" onclick="return confirm('Anda yakin ingin menghapus data ini?')"> Delete
                                </button>
                            </form>
                            @endif
                            
                        </td>
                      
                    </tr>
                    @endforeach                
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     
      <div class="modal-body">
        <form method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data">
          @csrf
          <div class="card">
            <div class="card-header">
              <h4>Input Data Objek</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label >Nama</label>
                <input type="text" class="form-control"  placeholder="Masukkan nama " name="name" required>
                
              </div>
              <div class="form-group">
                <label >Email</label>
                <input type="email" class="form-control"  placeholder="Masukkan Email " name="email" required>
                
              </div>
              <div class="form-group">
                <label >Password</label>
                <input type="password" class="form-control"  placeholder="Masukkan Password" name="password" required>
              </div>
              <div class="form-group">
                <label>Level Akses</label>
                <select class="custom-select" name="level" required>
                  <option selected>Pilih</option>
                  <option value="2">External Access</option>
                  <option value="0">Internal Access</option>
                </select>
              </div>
            </div>
            <div class="card-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     
      <div class="modal-body">
        <form method="POST" action="{{ route('ow.update', 0 ) }}" enctype="multipart/form-data">
          @method('PUT')
          @csrf
          <div class="card">
            <div class="card-header">
              <h4>Edit Data Objek</h4>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <div class="form-group">
                      <label >Nama</label>
                      <input type="text" class="form-control"  placeholder="Masukkan nama " id="name" name="name" required>
                      
                    </div>
                    <div class="form-group">
                      <label >Email</label>
                      <input type="email" class="form-control"  placeholder="Masukkan Email " id="email" name="email" required>
                      
                    </div>
                    <div class="form-group">
                      <label >Password</label>
                      <input type="password" class="form-control"  placeholder="Masukkan Password" name="password" required>
                     
                    </div>
                    
                  </div>
            </div>

            <div class="card-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>



@section('script')
<script>
  $('#edit').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id   = button.data('id')
  var name = button.data('name') // Extract info from data-* attributes
  var email = button.data('email')
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-body #id').val(id);
  modal.find('.modal-body #name').val(name);
  modal.find('.modal-body #email').val(email);
  

})

$('#tambah').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id   = button.data('id')
  var nama   = button.data('nama')
  
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-body #id').val(id);
  modal.find('.modal-body #nama').val(nama);

})






</script>
@endsection

@endsection