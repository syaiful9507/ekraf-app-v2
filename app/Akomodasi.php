<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akomodasi extends Model
{
    protected $table = "akomodasi";
    protected $fillable = ['nama','kamar','tdup','generated_at', 'chse', 'peduli_lindungi', 'whatsapp','migrate'];
    public function kunjungan()
    {
        return $this->hasMany('App\KunjunganAkomodasi', 'akomodasi_id', 'id');
    } 

    public static function akomodasi()
    {
        return Akomodasi::get()->count();
    }

    
    
}
