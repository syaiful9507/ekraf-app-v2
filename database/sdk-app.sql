-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 18 Des 2021 pada 07.42
-- Versi server: 10.4.18-MariaDB
-- Versi PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdk-app`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akomodasi`
--

CREATE TABLE `akomodasi` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `kamar` int(20) DEFAULT 0,
  `tdup` varchar(20) NOT NULL DEFAULT 'tidak ada',
  `generated_at` int(10) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akomodasi`
--

INSERT INTO `akomodasi` (`id`, `nama`, `kamar`, `tdup`, `generated_at`, `created_at`, `updated_at`) VALUES
(333, 'BIGLAND SENTUL HOTEL & CONVENTION', 146, 'Memiliki', 2021, NULL, '2021-12-15 03:59:22'),
(334, 'Royal Tulip Gunung Geulis Resort and Golf', 173, 'Memiliki', 2021, NULL, '2021-12-15 03:59:31'),
(335, 'Grand Cempaka Resort Convention ', 114, 'Memiliki', 0, NULL, NULL),
(336, 'Sentul 8 Hotel', 69, 'Memiliki', 0, NULL, NULL),
(337, 'Neo+ Green Savana Sentul City', 70, 'Memiliki', 0, NULL, NULL),
(338, 'HARRIS Sentul City Bogor', 160, 'Memiliki', 0, NULL, NULL),
(339, 'The Highland park Resort Hotel Bogor', 85, 'Memiliki', 2021, NULL, '2021-12-15 07:22:23'),
(340, 'Lestari Resort', 25, 'Dalam Proses', 0, NULL, NULL),
(341, 'Ahiyaksa Resort', 45, 'Dalam Proses', 0, NULL, NULL),
(342, 'Hotel Lurus Cisarua', 43, 'Dalam Proses', 0, NULL, NULL),
(343, 'JIMMERS HOTEL', 102, 'Dalam Proses', 0, NULL, NULL),
(344, 'JAMBULUWUK CONVENTION HALL & RESORT PUNCAK', 87, 'Memiliki', 0, NULL, NULL),
(345, 'GREEN PEAK', 139, 'Memiliki', 0, NULL, NULL),
(346, 'CISARUA INDAH', 554, 'Dalam Proses', 0, NULL, NULL),
(347, 'Taman Bukit Palem Resort', 149, 'Memiliki', 0, NULL, NULL),
(348, 'VILLA ANGGREK', 14, 'Memiliki', 0, NULL, NULL),
(349, 'HOTEL BONARINDO', 24, 'Memiliki', 0, NULL, NULL),
(350, 'PUNCAK RAYA HOTEL', 156, 'Memiliki', 0, NULL, NULL),
(351, 'BUENA VISTA HOTEL', 14, 'Memiliki', 0, NULL, NULL),
(352, 'ALFA RESORT', 31, 'Memiliki', 0, NULL, NULL),
(353, 'VILLA TJOKRO HOTEL', 62, 'Memiliki', 0, NULL, NULL),
(354, 'AMARSYA HOTEL', 24, 'Memiliki', 0, NULL, NULL),
(355, 'HIJAU PRATAMA HOTEL ', 19, 'Memiliki', 0, NULL, NULL),
(356, 'ROYAL SAFARI GARDEN HOTEL', 291, 'Memiliki', 0, NULL, NULL),
(357, 'GRAND DIARA HOTEL', 80, 'Memiliki', 0, NULL, NULL),
(358, 'HOTEL ARIANDI', 54, 'Memiliki', 0, NULL, NULL),
(359, 'RESORT PRIMA CISARUA ', 25, 'Memiliki', 0, NULL, NULL),
(360, 'HOTEL SAFARI LODGE', 106, 'Memiliki', 0, NULL, NULL),
(361, 'INDRA DJAYA HOTEL', 32, 'Memiliki', 0, NULL, NULL),
(362, 'NEW AYUDA 2 HOTEL ', 33, 'Memiliki', 0, NULL, NULL),
(363, 'NEW AYUDA PUNCAK', 102, 'Memiliki', 0, NULL, NULL),
(364, 'MEGA HOTEL & RESORT', 45, 'Memiliki', 0, NULL, NULL),
(365, 'HOTEL BAHTERA PELNI', 63, 'Memiliki', 0, NULL, NULL),
(366, 'Grand Cempaka Resort And Convention ', 114, 'Memiliki', 0, NULL, NULL),
(367, 'GUMILANG HOTEL', 54, 'Memiliki', 0, NULL, NULL),
(368, 'HOTEL ACCRAM', 45, 'Memiliki', 0, NULL, NULL),
(369, 'HOTEL BINTANG JADAYAT', 47, 'Tidak Memiliki', 0, NULL, NULL),
(370, 'HOTEL FITRIA', 16, 'Memiliki', 0, NULL, NULL),
(371, 'CIBEUREUM HOTEL', 25, 'Memiliki', 0, NULL, NULL),
(372, 'BOGOR INDAH NIRWANA', 24, 'Memiliki', 0, NULL, NULL),
(373, 'METROPOL HOTEL', 18, 'Memiliki', 0, NULL, NULL),
(374, 'HOTEL MARS \'91', 36, 'Memiliki', 0, NULL, NULL),
(375, 'NEW PURNAMA HOTEL', 42, 'Memiliki', 0, NULL, NULL),
(376, 'CIPAYUNG ASRI', 77, 'Dalam Proses', 0, NULL, NULL),
(377, 'BAYAK HOTEL', 60, 'Memiliki', 0, NULL, NULL),
(378, 'RIZEN PREMIER HOTEL', 117, 'Memiliki', 0, NULL, NULL),
(379, 'NARATAS HOTEL', 27, 'Memiliki', 0, NULL, NULL),
(380, 'BONITA HOTEL', 31, 'Memiliki', 0, NULL, NULL),
(381, 'GRIYA SAKINAH HOTEL', 17, 'Memiliki', 0, NULL, NULL),
(382, 'ARIES BIRU HOTEL', 110, 'Memiliki', 0, NULL, NULL),
(383, 'AQUARIUS ORANGE HOTEL', 40, 'Memiliki', 0, NULL, NULL),
(384, 'SRI INDRAWATI HOTEL', 29, 'Memiliki', 0, NULL, NULL),
(385, 'RUDIAN HOTEL', 42, 'Dalam Proses', 0, NULL, NULL),
(386, 'HOTEL DAVINCI', 22, 'Dalam Proses', 0, NULL, NULL),
(387, 'PARAMA HOTEL', 160, 'Memiliki', 0, NULL, NULL),
(388, 'TAMAN TERATAI HOTEL', 36, 'Memiliki', 0, NULL, NULL),
(389, 'PURI AYUDA RESORT', 70, 'Tidak Memiliki', 0, NULL, NULL),
(390, 'GERBERA HOTEL', 58, 'Memiliki', 0, NULL, NULL),
(391, 'LAMIN INDAH 1 HOTEL', 12, 'Memiliki', 0, NULL, NULL),
(392, 'LAMIN INDAH 2 HOTEL ', 10, 'Memiliki', 0, NULL, NULL),
(393, 'Hotel Citra Cikopo', 32, 'Memiliki', 0, NULL, NULL),
(394, 'HOTEL BALE ARIMBI', 62, 'Memiliki', 0, NULL, NULL),
(395, 'BELLA CAMPA HOTEL', 20, 'Memiliki', 0, NULL, NULL),
(396, '3G RESORT', 45, 'Memiliki', 0, NULL, NULL),
(397, 'HOTEL ANDALUS', 23, 'Memiliki', 0, NULL, NULL),
(398, 'HOTEL CIK MUNGIL', 36, 'Memiliki', 0, NULL, NULL),
(399, 'HOTEL GRIYA ASTOETI', 83, 'Memiliki', 0, NULL, NULL),
(400, 'BUDI LUHUR HOTEL', 10, 'Memiliki', 0, NULL, NULL),
(401, 'THE YONAN HOTEL', 20, 'Memiliki', 0, NULL, NULL),
(402, 'BRIA HOTEL & CONVENTION', 25, 'Memiliki', 0, NULL, NULL),
(403, 'CAHAYA VILLAGE', 38, 'Memiliki', 0, NULL, NULL),
(404, 'D\'AGAPE RESORT', 48, 'Memiliki', 0, NULL, NULL),
(405, 'HOTEL PESONA ANGGRAENI', 56, 'Memiliki', 0, NULL, NULL),
(406, 'NEW KARWIKA HOTEL & RESORT', 50, 'Memiliki', 0, NULL, NULL),
(407, 'Beth Kasegaran Theresia', 0, '', 0, NULL, NULL),
(408, 'WisataAgro PTPN VIII', 0, '', 0, NULL, NULL),
(409, 'Pesona Alam Resort & Spa', 0, '', 0, NULL, NULL),
(410, 'HOTEL LORIN SENTUL', 339, '', 0, NULL, NULL),
(411, 'Ole suites hotel & cottage', 88, '', 0, NULL, NULL),
(412, 'The Grand Hill Hotel', 84, '', 0, NULL, NULL),
(413, 'Villa Khayangan', 0, '', 0, NULL, NULL),
(414, 'Kedaton 8 Hotel, Restaurant & Business Lounge', 69, '', 0, NULL, NULL),
(415, 'Ole Suites Hotel', 50, '', 0, NULL, NULL),
(416, 'Pullman Ciawi Vimala Hills', 229, '', 0, NULL, NULL),
(417, 'Novotel Hotels And Resort', 179, '', 0, NULL, NULL),
(418, 'IBIS STYLES BOGOR RAYA', 205, '', 0, NULL, NULL),
(419, 'Lido Lake Resort by MNC Hotel', 100, '', 0, NULL, NULL),
(420, 'Grand Ussu Hotel and Convention', 100, '', 0, NULL, NULL),
(421, 'Gumilang Hotel', 54, '', 0, NULL, NULL),
(422, 'OLE SUITES HOTEL ', 50, '', 0, NULL, NULL),
(423, 'OLE SUITES COTTAGE', 88, '', 0, NULL, NULL),
(424, 'Resort Prima Cipayung', 30, '', 0, NULL, NULL),
(425, 'Hotel Nite and Day MDC Gadog', 84, '', 0, NULL, NULL),
(426, 'Hotel lembah nyiur', 67, '', 0, NULL, NULL),
(427, 'The Alana Hotel & Conference Center Sentul City', 271, '', 0, NULL, NULL),
(428, 'The Jayakarta Cisarua Inn & Villas', 31, '', 0, NULL, NULL),
(429, 'HOTEL PURNAMA', 40, '', 0, NULL, NULL),
(430, 'Agrowisata Gunung mas', 88, '', 0, NULL, NULL),
(431, 'RUKUN senior living', 60, '', 0, NULL, NULL),
(432, 'JSI Resort', 117, '', 0, NULL, NULL),
(433, 'Parama Hotel', 160, '', 0, NULL, NULL),
(434, 'Resort prima cisarua', 25, '', 0, NULL, NULL),
(435, 'Aston Sentul Lake Resort & Conference Center', 220, '', 0, NULL, NULL),
(436, 'Hotel Horison Ultima Bhuvana Ciawi', 220, '', 0, NULL, NULL),
(437, 'Leuweung Geledegan Ecolodge', 82, '', 0, NULL, NULL),
(438, 'Wisma Industri', 50, '', 0, NULL, NULL),
(439, 'Hotel Graha Dinar Syariah', 125, '', 0, NULL, NULL),
(440, 'HOTEL PURNAMA', 40, '', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktifitas`
--

CREATE TABLE `aktifitas` (
  `id` bigint(20) NOT NULL,
  `nama_aktifitas` text NOT NULL,
  `users_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kunjungan_akomodasi`
--

CREATE TABLE `kunjungan_akomodasi` (
  `id` bigint(20) NOT NULL,
  `bulan` date NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  `users_id` bigint(20) NOT NULL,
  `akomodasi_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kunjungan_akomodasi`
--

INSERT INTO `kunjungan_akomodasi` (`id`, `bulan`, `jumlah`, `users_id`, `akomodasi_id`, `created_at`, `updated_at`) VALUES
(89, '2020-01-01', 80, 1, 1, NULL, '2020-12-14 02:43:13'),
(90, '2020-02-01', 0, 1, 1, NULL, NULL),
(91, '2020-03-01', 0, 1, 1, NULL, NULL),
(92, '2020-04-01', 0, 1, 1, NULL, NULL),
(93, '2020-05-01', 0, 1, 1, NULL, NULL),
(94, '2020-06-01', 0, 1, 1, NULL, NULL),
(95, '2020-07-01', 0, 1, 1, NULL, NULL),
(96, '2020-08-01', 0, 1, 1, NULL, NULL),
(97, '2020-09-01', 0, 1, 1, NULL, NULL),
(98, '2020-10-01', 0, 1, 1, NULL, NULL),
(99, '2020-11-01', 0, 1, 1, NULL, NULL),
(100, '2020-12-01', 0, 1, 1, NULL, NULL),
(101, '2020-01-01', 0, 1, 2, NULL, NULL),
(102, '2020-02-01', 0, 1, 2, NULL, NULL),
(103, '2020-03-01', 0, 1, 2, NULL, NULL),
(104, '2020-04-01', 0, 1, 2, NULL, NULL),
(105, '2020-05-01', 0, 1, 2, NULL, NULL),
(106, '2020-06-01', 0, 1, 2, NULL, NULL),
(107, '2020-07-01', 0, 1, 2, NULL, NULL),
(108, '2020-08-01', 0, 1, 2, NULL, NULL),
(109, '2020-09-01', 0, 1, 2, NULL, NULL),
(110, '2020-10-01', 0, 1, 2, NULL, NULL),
(111, '2020-11-01', 0, 1, 2, NULL, NULL),
(112, '2020-12-01', 0, 1, 2, NULL, NULL),
(113, '2020-01-01', 0, 1, 3, NULL, NULL),
(114, '2020-02-01', 0, 1, 3, NULL, NULL),
(115, '2020-03-01', 0, 1, 3, NULL, NULL),
(116, '2020-04-01', 0, 1, 3, NULL, NULL),
(117, '2020-05-01', 0, 1, 3, NULL, NULL),
(118, '2020-06-01', 0, 1, 3, NULL, NULL),
(119, '2020-07-01', 0, 1, 3, NULL, NULL),
(120, '2020-08-01', 0, 1, 3, NULL, NULL),
(121, '2020-09-01', 0, 1, 3, NULL, NULL),
(122, '2020-10-01', 0, 1, 3, NULL, NULL),
(123, '2020-11-01', 0, 1, 3, NULL, NULL),
(124, '2020-12-01', 0, 1, 3, NULL, NULL),
(125, '2020-01-01', 0, 1, 4, NULL, NULL),
(126, '2020-02-01', 0, 1, 4, NULL, NULL),
(127, '2020-03-01', 0, 1, 4, NULL, NULL),
(128, '2020-04-01', 0, 1, 4, NULL, NULL),
(129, '2020-05-01', 0, 1, 4, NULL, NULL),
(130, '2020-06-01', 0, 1, 4, NULL, NULL),
(131, '2020-07-01', 0, 1, 4, NULL, NULL),
(132, '2020-08-01', 0, 1, 4, NULL, NULL),
(133, '2020-09-01', 0, 1, 4, NULL, NULL),
(134, '2020-10-01', 0, 1, 4, NULL, NULL),
(135, '2020-11-01', 0, 1, 4, NULL, NULL),
(136, '2020-12-01', 0, 1, 4, NULL, NULL),
(137, '2021-01-01', 0, 1, 1, NULL, NULL),
(138, '2021-02-01', 0, 1, 1, NULL, NULL),
(139, '2021-03-01', 0, 1, 1, NULL, NULL),
(140, '2021-04-01', 0, 1, 1, NULL, NULL),
(141, '2021-05-01', 0, 1, 1, NULL, NULL),
(142, '2021-06-01', 0, 1, 1, NULL, NULL),
(143, '2021-07-01', 0, 1, 1, NULL, NULL),
(144, '2021-08-01', 0, 1, 1, NULL, NULL),
(145, '2021-09-01', 0, 1, 1, NULL, NULL),
(146, '2021-10-01', 0, 1, 1, NULL, NULL),
(147, '2021-11-01', 0, 1, 1, NULL, NULL),
(148, '2021-12-01', 0, 1, 1, NULL, NULL),
(149, '2021-01-01', 0, 1, 2, NULL, NULL),
(150, '2021-02-01', 0, 1, 2, NULL, NULL),
(151, '2021-03-01', 0, 1, 2, NULL, NULL),
(152, '2021-04-01', 0, 1, 2, NULL, NULL),
(153, '2021-05-01', 0, 1, 2, NULL, NULL),
(154, '2021-06-01', 0, 1, 2, NULL, NULL),
(155, '2021-07-01', 0, 1, 2, NULL, NULL),
(156, '2021-08-01', 0, 1, 2, NULL, NULL),
(157, '2021-09-01', 0, 1, 2, NULL, NULL),
(158, '2021-10-01', 0, 1, 2, NULL, NULL),
(159, '2021-11-01', 0, 1, 2, NULL, NULL),
(160, '2021-12-01', 0, 1, 2, NULL, NULL),
(161, '2021-01-01', 0, 1, 3, NULL, NULL),
(162, '2021-02-01', 0, 1, 3, NULL, NULL),
(163, '2021-03-01', 0, 1, 3, NULL, NULL),
(164, '2021-04-01', 0, 1, 3, NULL, NULL),
(165, '2021-05-01', 0, 1, 3, NULL, NULL),
(166, '2021-06-01', 0, 1, 3, NULL, NULL),
(167, '2021-07-01', 0, 1, 3, NULL, NULL),
(168, '2021-08-01', 0, 1, 3, NULL, NULL),
(169, '2021-09-01', 0, 1, 3, NULL, NULL),
(170, '2021-10-01', 0, 1, 3, NULL, NULL),
(171, '2021-11-01', 0, 1, 3, NULL, NULL),
(172, '2021-12-01', 0, 1, 3, NULL, NULL),
(173, '2021-01-01', 0, 1, 4, NULL, NULL),
(174, '2021-02-01', 0, 1, 4, NULL, NULL),
(175, '2021-03-01', 0, 1, 4, NULL, NULL),
(176, '2021-04-01', 0, 1, 4, NULL, NULL),
(177, '2021-05-01', 0, 1, 4, NULL, NULL),
(178, '2021-06-01', 0, 1, 4, NULL, NULL),
(179, '2021-07-01', 0, 1, 4, NULL, NULL),
(180, '2021-08-01', 0, 1, 4, NULL, NULL),
(181, '2021-09-01', 0, 1, 4, NULL, NULL),
(182, '2021-10-01', 0, 1, 4, NULL, NULL),
(183, '2021-11-01', 0, 1, 4, NULL, NULL),
(184, '2021-12-01', 0, 1, 4, NULL, NULL),
(185, '2021-01-01', 0, 1, 333, NULL, NULL),
(186, '2021-02-01', 0, 1, 333, NULL, NULL),
(187, '2021-03-01', 0, 1, 333, NULL, NULL),
(188, '2021-04-01', 0, 1, 333, NULL, NULL),
(189, '2021-05-01', 0, 1, 333, NULL, NULL),
(190, '2021-06-01', 0, 1, 333, NULL, NULL),
(191, '2021-07-01', 0, 1, 333, NULL, NULL),
(192, '2021-08-01', 0, 1, 333, NULL, NULL),
(193, '2021-09-01', 0, 1, 333, NULL, NULL),
(194, '2021-10-01', 0, 1, 333, NULL, NULL),
(195, '2021-11-01', 0, 1, 333, NULL, NULL),
(196, '2021-12-01', 0, 1, 333, NULL, NULL),
(197, '2021-01-01', 0, 1, 334, NULL, NULL),
(198, '2021-02-01', 0, 1, 334, NULL, NULL),
(199, '2021-03-01', 0, 1, 334, NULL, NULL),
(200, '2021-04-01', 0, 1, 334, NULL, NULL),
(201, '2021-05-01', 90, 1, 334, NULL, '2021-12-15 07:21:40'),
(202, '2021-06-01', 0, 1, 334, NULL, NULL),
(203, '2021-07-01', 0, 1, 334, NULL, NULL),
(204, '2021-08-01', 0, 1, 334, NULL, NULL),
(205, '2021-09-01', 0, 1, 334, NULL, NULL),
(206, '2021-10-01', 0, 1, 334, NULL, NULL),
(207, '2021-11-01', 0, 1, 334, NULL, NULL),
(208, '2021-12-01', 0, 1, 334, NULL, NULL),
(209, '2021-01-01', 0, 1, 339, NULL, NULL),
(210, '2021-02-01', 0, 1, 339, NULL, NULL),
(211, '2021-03-01', 0, 1, 339, NULL, NULL),
(212, '2021-04-01', 0, 1, 339, NULL, NULL),
(213, '2021-05-01', 0, 1, 339, NULL, NULL),
(214, '2021-06-01', 0, 1, 339, NULL, NULL),
(215, '2021-07-01', 0, 1, 339, NULL, NULL),
(216, '2021-08-01', 0, 1, 339, NULL, NULL),
(217, '2021-09-01', 0, 1, 339, NULL, NULL),
(218, '2021-10-01', 0, 1, 339, NULL, NULL),
(219, '2021-11-01', 0, 1, 339, NULL, NULL),
(220, '2021-12-01', 0, 1, 339, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kunjungan_objek_wisata`
--

CREATE TABLE `kunjungan_objek_wisata` (
  `id` bigint(20) NOT NULL,
  `bulan` date NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  `jumlah_asing` bigint(20) NOT NULL DEFAULT 0,
  `users_id` bigint(20) NOT NULL,
  `objek_wisata_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kunjungan_objek_wisata`
--

INSERT INTO `kunjungan_objek_wisata` (`id`, `bulan`, `jumlah`, `jumlah_asing`, `users_id`, `objek_wisata_id`, `created_at`, `updated_at`) VALUES
(177, '2020-01-01', 80, 0, 1, 1, NULL, '2020-12-14 02:43:42'),
(178, '2020-02-01', 0, 0, 1, 1, NULL, NULL),
(179, '2020-03-01', 0, 0, 1, 1, NULL, NULL),
(180, '2020-04-01', 0, 0, 1, 1, NULL, NULL),
(181, '2020-05-01', 0, 0, 1, 1, NULL, NULL),
(182, '2020-06-01', 0, 0, 1, 1, NULL, NULL),
(183, '2020-07-01', 0, 0, 1, 1, NULL, NULL),
(184, '2020-08-01', 0, 0, 1, 1, NULL, NULL),
(185, '2020-09-01', 0, 0, 1, 1, NULL, NULL),
(186, '2020-10-01', 0, 0, 1, 1, NULL, NULL),
(187, '2020-11-01', 0, 0, 1, 1, NULL, NULL),
(188, '2020-12-01', 0, 0, 1, 1, NULL, NULL),
(189, '2020-01-01', 90, 0, 1, 2, NULL, '2021-12-15 02:46:02'),
(190, '2020-02-01', 0, 0, 1, 2, NULL, NULL),
(191, '2020-03-01', 90, 0, 1, 2, NULL, '2021-12-15 02:46:14'),
(192, '2020-04-01', 0, 0, 1, 2, NULL, NULL),
(193, '2020-05-01', 78, 28, 1, 2, NULL, '2021-12-15 02:46:28'),
(194, '2020-06-01', 0, 0, 1, 2, NULL, NULL),
(195, '2020-07-01', 0, 0, 1, 2, NULL, NULL),
(196, '2020-08-01', 0, 0, 1, 2, NULL, NULL),
(197, '2020-09-01', 0, 0, 1, 2, NULL, NULL),
(198, '2020-10-01', 0, 0, 1, 2, NULL, NULL),
(199, '2020-11-01', 0, 0, 1, 2, NULL, NULL),
(200, '2020-12-01', 0, 0, 1, 2, NULL, NULL),
(201, '2021-01-01', 0, 0, 1, 1, NULL, NULL),
(202, '2021-02-01', 0, 0, 1, 1, NULL, NULL),
(203, '2021-03-01', 0, 0, 1, 1, NULL, NULL),
(204, '2021-04-01', 0, 0, 1, 1, NULL, NULL),
(205, '2021-05-01', 0, 0, 1, 1, NULL, NULL),
(206, '2021-06-01', 0, 0, 1, 1, NULL, NULL),
(207, '2021-07-01', 0, 0, 1, 1, NULL, NULL),
(208, '2021-08-01', 0, 0, 1, 1, NULL, NULL),
(209, '2021-09-01', 0, 0, 1, 1, NULL, NULL),
(210, '2021-10-01', 0, 0, 1, 1, NULL, NULL),
(211, '2021-11-01', 0, 0, 1, 1, NULL, NULL),
(212, '2021-12-01', 0, 0, 1, 1, NULL, NULL),
(213, '2021-01-01', 0, 0, 1, 2, NULL, NULL),
(214, '2021-02-01', 0, 0, 1, 2, NULL, NULL),
(215, '2021-03-01', 0, 0, 1, 2, NULL, NULL),
(216, '2021-04-01', 0, 0, 1, 2, NULL, NULL),
(217, '2021-05-01', 0, 0, 1, 2, NULL, NULL),
(218, '2021-06-01', 0, 0, 1, 2, NULL, NULL),
(219, '2021-07-01', 0, 0, 1, 2, NULL, NULL),
(220, '2021-08-01', 0, 0, 1, 2, NULL, NULL),
(221, '2021-09-01', 0, 0, 1, 2, NULL, NULL),
(222, '2021-10-01', 0, 0, 1, 2, NULL, NULL),
(223, '2021-11-01', 0, 0, 1, 2, NULL, NULL),
(224, '2021-12-01', 0, 0, 1, 2, NULL, NULL),
(225, '2021-01-01', 354, 0, 8, 3, NULL, '2021-12-15 09:12:35'),
(226, '2021-02-01', 0, 0, 8, 3, NULL, NULL),
(227, '2021-03-01', 0, 0, 8, 3, NULL, NULL),
(228, '2021-04-01', 0, 0, 8, 3, NULL, NULL),
(229, '2021-05-01', 0, 0, 8, 3, NULL, NULL),
(230, '2021-06-01', 0, 0, 8, 3, NULL, NULL),
(231, '2021-07-01', 0, 0, 8, 3, NULL, NULL),
(232, '2021-08-01', 0, 0, 8, 3, NULL, NULL),
(233, '2021-09-01', 0, 0, 8, 3, NULL, NULL),
(234, '2021-10-01', 0, 0, 8, 3, NULL, NULL),
(235, '2021-11-01', 0, 0, 8, 3, NULL, NULL),
(236, '2021-12-01', 0, 0, 8, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `objek_wisata`
--

CREATE TABLE `objek_wisata` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `generated_at` int(10) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `objek_wisata`
--

INSERT INTO `objek_wisata` (`id`, `nama`, `generated_at`, `created_at`, `updated_at`) VALUES
(3, 'Cibalung happy land', 2021, NULL, '2021-12-15 09:11:06'),
(4, 'PT Taman Wisata Matahari', 0, NULL, NULL),
(5, 'pusat pendidikan konservasi alam bodogol', 0, NULL, NULL),
(6, 'Curug Cilember', 0, NULL, NULL),
(7, 'Curug putri pelangi', 0, NULL, NULL),
(8, 'TAMAN SAFARI INDONESIA', 0, NULL, NULL),
(9, 'Melrimba Garden Puncak', 0, NULL, NULL),
(10, 'WARSO FARM', 0, NULL, NULL),
(11, 'Melrimba garden puncak', 0, NULL, NULL),
(12, 'Curug Kembar Batulayang', 0, NULL, NULL),
(13, 'Buper Citamiang', 0, NULL, NULL),
(14, 'Curug Cipamingkis', 0, NULL, NULL),
(15, 'Curug Ciherang', 0, NULL, NULL),
(16, 'Curug Leuwihejo ', 0, NULL, NULL),
(17, 'Curug Cibeureum', 0, NULL, NULL),
(18, 'Curug Barong Leuwi Hejo', 0, NULL, NULL),
(19, 'Track Sepeda Puncak Kondang', 0, NULL, NULL),
(20, 'Curug Putri Kencana', 0, NULL, NULL),
(21, 'Wisata Alam Baru Jeruk', 0, NULL, NULL),
(22, 'Wisata Alam Gunung Kencana', 0, NULL, NULL),
(23, 'PUNCAK LANGIT', 0, NULL, NULL),
(24, 'CRG BARONG CIBADAK', 0, NULL, NULL),
(25, 'CRG GORDENG', 0, NULL, NULL),
(26, 'BUPER CISARUA', 0, NULL, NULL),
(27, 'GOA GARUNGGANG', 0, NULL, NULL),
(28, 'CURUG CIBINGBIN', 0, NULL, NULL),
(29, 'JUNGLE CAMP', 0, NULL, NULL),
(30, 'PASEBAN', 0, NULL, NULL),
(31, 'CISUREN', 0, NULL, NULL),
(32, 'GUNUNG LUHUR', 0, NULL, NULL),
(33, 'CURUG MARUK', 0, NULL, NULL),
(34, 'TALAGA SAAT', 0, NULL, NULL),
(35, 'CIMANDALA', 0, NULL, NULL),
(36, 'CITRA ALAM PASEBAN', 0, NULL, NULL),
(37, 'Kampung Wisata Cinangneng ', 0, NULL, NULL),
(38, 'Kawasan wisata gunung salak Endah', 0, NULL, NULL),
(39, 'Pamandian air panas ( G.S.E )', 0, NULL, NULL),
(40, 'Curug cigamea ( G.S.E )', 0, NULL, NULL),
(41, 'Curug putri pelangi', 0, NULL, NULL),
(42, 'Setu kembar ( G.S.E )', 0, NULL, NULL),
(43, 'Curug seribu ( G.S.E )', 0, NULL, NULL),
(44, 'Curug muara herang ( G.S.E)', 0, NULL, NULL),
(45, 'Curug Ratu ( G.S.E )', 0, NULL, NULL),
(46, 'Curug kondang ( G.S.E)', 0, NULL, NULL),
(47, 'Curug goa alami ( G.S.E )', 0, NULL, NULL),
(48, 'Curug pasirengit ( G.S.E)', 0, NULL, NULL),
(49, 'Curug pangeran (G.S.E )', 0, NULL, NULL),
(50, 'Curug lembah Tepus (G.S.E )', 0, NULL, NULL),
(51, 'Balong endah ( G.S.E )', 0, NULL, NULL),
(52, 'Setu batu ( G.S.E )', 0, NULL, NULL),
(53, 'Kolam renang Marinas 2', 0, NULL, NULL),
(54, 'TELAGA WARNA ', 0, NULL, NULL),
(55, 'AGROWISATA BUKIT HAMBALANG ', 0, NULL, NULL),
(56, 'Villa Khayangan ', 0, NULL, NULL),
(57, 'Rumah Ibu Waterboom', 0, NULL, NULL),
(58, 'Green canyon cariu bogor', 0, NULL, NULL),
(59, 'Wana Wisata Curug Cilember', 0, NULL, NULL),
(60, 'TWM Park', 0, NULL, NULL),
(61, 'Agrowisata Gunung mas', 0, NULL, NULL),
(62, 'CIBALUNG HAPPY LAND', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(5) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'SYAIFUL', 'syaiful@gmail.com', NULL, '$2y$10$.TaVCI.1IXmxM44Hw3XO4eGLx43g9Sz3npQUqOc9g8vHOjuWVOHxW', 1, NULL, '2021-12-11 23:21:22', '2021-12-11 23:21:22'),
(8, 'INDAH', 'ws@gmail.com', NULL, '$2y$10$b9ML/.IqF5cD9b/l1BMvR.GRjS3aY91v2CCf6.smLOhPZ0LTWygty', 0, NULL, '2021-12-15 08:02:29', '2021-12-15 09:27:37'),
(16, 'trhrg', 'admin@gmail.com', NULL, '$2y$10$hjEueW66YJ1teWXJdeRUQO/CMZpOc5Wiwi6wN37HqHK1t6tUyguFy', 0, NULL, '2021-12-15 08:41:49', '2021-12-15 08:41:49');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `akomodasi`
--
ALTER TABLE `akomodasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kunjungan_akomodasi`
--
ALTER TABLE `kunjungan_akomodasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kunjungan_objek_wisata`
--
ALTER TABLE `kunjungan_objek_wisata`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `objek_wisata`
--
ALTER TABLE `objek_wisata`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `akomodasi`
--
ALTER TABLE `akomodasi`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=441;

--
-- AUTO_INCREMENT untuk tabel `aktifitas`
--
ALTER TABLE `aktifitas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kunjungan_akomodasi`
--
ALTER TABLE `kunjungan_akomodasi`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;

--
-- AUTO_INCREMENT untuk tabel `kunjungan_objek_wisata`
--
ALTER TABLE `kunjungan_objek_wisata`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=237;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `objek_wisata`
--
ALTER TABLE `objek_wisata`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
