<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KunjunganOw extends Model
{
    protected $table = "kunjungan_objek_wisata";
    protected $fillable = ['bulan', 'jumlah','jumlah_asing', 'users_id', 'objek_wisata_id'];

    public function ow()
    {
        return $this->belongsTo('App\Ow','objek_wisata_id', 'id');
    }
 
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function thisYear()
    {
        $thisYear = date('2021');
        return DB::table('kunjungan_objek_wisata')->whereYear(DB::raw('bulan'), '=',$thisYear)->sum(DB::raw('jumlah'));
        
    }

    public static function thisMonth()
    {
        $thisMonth = date('m');
        return DB::table('kunjungan_objek_wisata')->whereMonth(DB::raw('bulan'), '=',$thisMonth)->sum(DB::raw('jumlah + jumlah_asing'));
    }
}
