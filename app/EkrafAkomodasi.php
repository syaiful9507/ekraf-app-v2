<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkrafAkomodasi extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'akomodasi';
    protected $fillable = ['akomodasi_nama','akomodasi_kamar','akomodasi_jenis','akomodasi_klasifikasi','akomodasi_alamat','akomodasi_kecamatan',
    'akomodasi_desa','akomodasi_kepemilikan','akomodasi_whatsapp',
    'akomodasi_tahun_berdiri','akomodasi_manajemen','akomodasi_chse','akomodasi_hibah','akomodasi_vaksin','akomodasi_tdup','akomodasi_keterangan','generated_at','users_id'
    ];

    
    
}
