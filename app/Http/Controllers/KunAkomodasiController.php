<?php

namespace App\Http\Controllers;

use App\Akomodasi;
use App\Exports\KunjunganAkomodasiExport;
use App\KunjunganAkomodasi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class KunAkomodasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*$bulan = $request->get('bulan').'-01';
        $bulan =date('Y-m-d',strtotime($bulan));
        $alertmont = date('F',strtotime($bulan));
        $search = KunjunganAkomodasi::where('bulan', $bulan)->where('akomodasi_id', $request->get('id'));
        if ($search->count() > 0) {
            Alert::error('Gagal', ' Bulan '.$alertmont.' Telah ada');
            return redirect()->back();
        } else {
            $kunjunganAkomodasi = new KunjunganAkomodasi();
            $kunjunganAkomodasi->bulan = $bulan;
            $kunjunganAkomodasi->jumlah = $request->get('jumlah');
            $kunjunganAkomodasi->akomodasi_id = $request->get('id');
            $kunjunganAkomodasi->users_id = Auth::user()->id;
            $kunjunganAkomodasi->save();
            Alert::success('Sukses', 'Kunjungan Berhasil ditambah');
            return redirect()->back();
            
        }*/

        $curyear = date('Y');
        $data =[
            ['bulan' => $curyear.'-01-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-02-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-03-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-04-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-05-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-06-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-07-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-08-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-09-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-10-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-11-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
            ['bulan' => $curyear.'-12-01', 'jumlah' => 0, 'akomodasi_id'=> $request->get('id'), 'users_id'=> Auth::user()->id ],
        ];

        KunjunganAkomodasi::insert($data);

        $user = Akomodasi::find($request->get('id'));
        $user->generated_at = $curyear;
        $user->save();

        Alert::success('Sukses', 'Generate');
        return redirect()->back();
        
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$curyear = date('Y');
        $curyear = date('2021');
        $akomodasi = Akomodasi::find($id);
        $datas = KunjunganAkomodasi::where('akomodasi_id', $id)->orderBy('bulan', 'asc')->get();
        /*$datas = DB::table('kunjungan_akomodasi')->where(DB::raw('akomodasi_id'),'=', $id)->whereYear(DB::raw('bulan'), '=',$curyear)->select([
            DB::raw('SUM(jumlah) as jumlah'),
            DB::raw('bulan as bulan'),
        ])->orderBy('bulan', 'asc')->groupBy('bulan')->orderBy('id', 'desc')->get();
        */

        $KunThisYear = KunjunganAkomodasi::where('akomodasi_id', $id)->whereYear('bulan', $curyear)->sum('jumlah');
        $avgPerMonth = KunjunganAkomodasi::where('akomodasi_id', $id)->whereYear('bulan', $curyear)->pluck('jumlah')->avg();
        $avgPerMonth = number_format($avgPerMonth);
        $record = KunjunganAkomodasi::select([
            DB::raw(DB::raw("DATE_FORMAT(bulan, '%M') as bulan")),
            DB::raw(DB::raw('jumlah as jumlah'))
        ])->where('akomodasi_id', $id)->where(DB::raw("DATE_FORMAT(bulan, '%Y')", $curyear ), $curyear)->orderBy(DB::raw("DATE_FORMAT(bulan, '%m')", 'asc' ))->get();

        $data = [];
        foreach ($record as $row) {
            # code...
            $data['label'][] = $row->bulan;
            $data['data'][] = (int) $row->jumlah;
        }
        $data['chart_data'] = json_encode($data);

        return view('akomodasi.kunjungan.index')->with($data)->with('datas', $datas)->with('akomodasi', $akomodasi)->with('KunThisYear', $KunThisYear)->with('avgPerMonth', $avgPerMonth);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $id = $request->get('id');
        $kunAkomodasi = KunjunganAkomodasi::find($id);
        $kunAkomodasi->jumlah = $request->get('jumlah');
        $kunAkomodasi->save();
        Alert::success('Sukses', 'Updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function exportAkomodasi($tahun)
    {
        /*$datas = Ow::with(['kunjungan' => function($query) {
            $query->whereYear('bulan', date('Y'));
        }])->get();
        return view('tested.index', compact('datas'));
        */

        $namafile = 'KUNJUNGAN_TAHUN_'.$tahun.''.'.xlsx';
        return Excel::download(new KunjunganAkomodasiExport($tahun), $namafile);
    }
}
