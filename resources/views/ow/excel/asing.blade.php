
<html>
    <head>
      <title>LAPORAN KUNJUNGAN</title>
      
      <style>
      
      table, td, th {
      border: 1px solid black;
    }
    
    table {
      border-collapse: collapse;
      width: 100%;
    }
    
    th {
      height: 50px;
    }
      </style>
    
    </head>
        <body>
        <table align="center">
                    <th colspan="17" rowspan=2 height="20" bgcolor="#0089c4" align="center">
              <b>DATA KUNJUNGAN
              <br>
              LAPORAN KUNJUNGAN ASING
            </b>
                </th>
        </table>
    <br>
            <table >
                
                <tr>
                <th width="20"  height="25" align="center" bgcolor="lightblue">Nama</th>
            <th width="20"  height="25" align="center" bgcolor="lightblue">WhatsApp</th>
            <th width="20"  height="25" align="center" bgcolor="lightblue">TDUP</th>
            <th width="20"  height="25" align="center" bgcolor="lightblue">CHSE</th>
            <th width="20"  height="25" align="center" bgcolor="lightblue">Peduli Lindungi</th>
                <th width="15"  height="25" align="center" bgcolor="lightblue">January</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">February</th>
            <td width="10"  height="25" align="center" bgcolor="lightblue">March</td>
            <th width="15"  height="25" align="center" bgcolor="lightblue">April</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">May</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">June</th>
                    <th width="15"  height="25" align="center" bgcolor="lightblue">July</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">August</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">September</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">October</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">November</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">December</th>
          </tr>
    
          @foreach ($datas as $item)
          <tr align="center">
            <td >{{$item->nama}}</td>
            <td >{{$item->whatsapp}}</td>
            <td >{{$item->tdup}}</td>
            <td >{{$item->chse}}</td>
            <td >{{$item->peduli_lindungi}}</td>
                              @foreach ($item->kunjungan->chunk(12) as $kun)
                                @foreach ($kun->sortBy('bulan') as $k)
                                      <td align="center">{{$k->jumlah_asing }}</td>
                                @endforeach
                              @endforeach
                
          </tr>
          @endforeach
    
          
            </table>
        </body>
    </html>