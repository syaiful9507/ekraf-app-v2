<?php

use App\Http\Controllers\AkomodasiController;
use App\Http\Controllers\EkrafController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KunAkomodasiController;
use App\Http\Controllers\KunOwController;
use App\Http\Controllers\KunRhuController;
use App\Http\Controllers\OwController;
use App\Http\Controllers\RhuController;
use App\Http\Controllers\UserController;
use App\KunjunganAkomodasi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');
    //##################AKOMODASI#######################
    Route::resource('akomodasi', 'AkomodasiController');
    Route::resource('k_akomodasi', 'KunAkomodasiController');
    Route::get('exportakomodasi/{tahun}', 'KunAkomodasiController@exportAkomodasi')->name('exportakomodasi');
    Route::post('akomodasi.migration', 'AkomodasiController@migration')->name('akomodasi.migration');

    //########### END OF AKOMODASI ROUTE ################

    //############################ OW ######################
    Route::resource('ow', 'OwController');
    Route::resource('k_ow', 'KunOwController');
    Route::get('exportow/{tahun}', 'KunOwController@exportOw')->name('exportow');
    Route::get('exportowasing/{tahun}', 'KunOwController@exportOwAsing')->name('exportowasing');
    Route::post('ow.migration', 'OwController@migration')->name('ow.migration');

    //############## END OF OW ####################

    //########## RHU #############
    Route::resource('rhu', 'RhuController');
    Route::resource('k_rhu', 'KunRhuController');
    Route::get('exportrhu/{tahun}', 'KunRhuController@exportRhu')->name('exportrhu');
    //########## END OF RHU ########

    //########### EKRAF ##################
    Route::resource('ekraf', 'EkrafController')->except('index');
    route::get('ekraf_index/{id}', 'EkrafController@index')->name('ekraf_index');
    Route::get('kurasi/{id}/{code}', 'EkrafController@kurasi')->name('kurasi');
    Route::get('export/{code}','EkrafController@export')->name('ekraf.export');
    Route::post('city', 'HomeController@city')->name('city');
    Route::post('district', 'HomeController@district')->name('district');
    Route::post('village', 'HomeController@village')->name('village');
    //######### END EKRAF ####################
    Route::resource('user', 'UserController');
});

