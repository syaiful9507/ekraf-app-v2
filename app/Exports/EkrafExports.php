<?php

namespace App\Exports;

use App\EkrafEkraf;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class EkrafExports implements FromView
{
    protected $code;
    function __construct($code)
    {
        $this->code = $code;
        
    }
    public function view(): View
    {
        return view('ekraf.excel.index',[
            'datas' => EkrafEkraf::where('kurasi', $this->code)->get()

        ]);
    }
}
