<?php

namespace App\Http\Controllers;

use App\Akomodasi;
use App\EkrafAkomodasi;
use App\EkrafAkomodasiKunjungan;
use App\KunjunganAkomodasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use RealRashid\SweetAlert\Facades\Toast;

class AkomodasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $curyear = date('2021'); 
        //$curyear = date('Y'); 
        $datas = Akomodasi::where('migrate', 0)->get();
        $record = KunjunganAkomodasi::select([
            DB::raw(DB::raw("DATE_FORMAT(bulan, '%M') as bulan")),
            DB::raw(DB::raw('SUM(jumlah) as jumlah'))
        ])->where(DB::raw("DATE_FORMAT(bulan, '%Y')" ), $curyear)->groupBy('bulan')->get();

        $data = [];
        foreach ($record as $row) {
            # code...
            $data['label'][] = $row->bulan;
            $data['data'][] = (int) $row->jumlah;
        }
        $data['chart_data'] = json_encode($data);
        $kunjunganTahun = KunjunganAkomodasi::selectRaw('year(bulan) year, count(*) data')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();

        $ekrafakomodasi = EkrafAkomodasi::where('migrate', 0)->get();

        return view('akomodasi.index')->with($data)->with('datas', $datas)->with('kunjunganTahun', $kunjunganTahun)->with('ekrafakomodasi', $ekrafakomodasi);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $akomodasi = new Akomodasi();
        $akomodasi->nama = $request->get('nama');
        $akomodasi->kamar = $request->get('kamar');
        $akomodasi->tdup = $request->get('tdup');
        $akomodasi->chse = $request->get('chse');
        $akomodasi->peduli_lindungi = $request->get('peduli');
        $akomodasi->whatsapp = $request->get('whatsapp');
        $akomodasi->save();
        Alert::success('Sukses', 'Tersimpan');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Akomodasi::find($id);
        return response()->json($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->get('id');
        $akomodasi = Akomodasi::find($id);
        $akomodasi->nama = $request->get('nama');
        $akomodasi->kamar = $request->get('kamar');
        $akomodasi->tdup = $request->get('tdup');
        $akomodasi->chse = $request->get('chse');
        $akomodasi->peduli_lindungi = $request->get('peduli');
        $akomodasi->whatsapp = $request->get('whatsapp');
        $akomodasi->save();
        Alert::success('Sukses', 'Updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function migration(Request $request)
    {
        $code = $request->CODE;
        $id = $request->id;
        $idakomodasiEkraf = $request->idakomodasiEkraf;
        

        if ($code === 'EXIST') {
            
            //DATA UDAH ADA 
            $akomodasi = Akomodasi::find($id);
            if ($akomodasi->tdup == 'Memiliki' OR $akomodasi->tdup == 'Ya') {$tdup = 1;} else {$tdup = 0;}
            if ($akomodasi->chse == 'Memiliki' OR $akomodasi->chse == 'Ya') {$chse = 1;} else {$chse = 0;}
            if ($akomodasi->peduli_lindungi == 'Memiliki' OR $akomodasi->peduli_lindungi == 'Ya') {$peduli = 1;} else {$peduli = 0;}

            $akomodasiEkrafINS = EkrafAkomodasi::find($idakomodasiEkraf);
            $akomodasiEkrafINS->akomodasi_tdup = $tdup;
            $akomodasiEkrafINS->akomodasi_chse = $chse;
            $akomodasiEkrafINS->akomodasi_vaksin = $peduli;
            $akomodasiEkrafINS->akomodasi_whatsapp = $akomodasi->whatsapp;
            $akomodasiEkrafINS->generated_at = $akomodasi->generated_at;
            $akomodasiEkrafINS->migrate = 1;
            $akomodasiEkrafINS->users_id = Auth::user()->id;
            $akomodasiEkrafINS->save();

            $kunjunagan = KunjunganAkomodasi::where('akomodasi_id', $id)->get();
            $coll = collect($kunjunagan);
            foreach ($coll as $coll) {
                # code...
                $data =[
                    ['bulan' => $coll->bulan, 'jumlah' => $coll->jumlah, 'akomodasi_id'=> $idakomodasiEkraf, 'users_id'=> Auth::user()->id ],
                   ];
                   EkrafAkomodasiKunjungan::insert($data);
            }
            
            //UPDATE AKOMODASI SDK
            $akomodasi->migrate = 1;
            $akomodasi->save();
            toast('Migration Success', 'success');
            return redirect()->back();

        } else {
            //DATA BARU
            $akomodasi = Akomodasi::find($id);
            if ($akomodasi->tdup == 'Memiliki' OR $akomodasi->tdup == 'Ya') {$tdup = 1;} else {$tdup = 0;}
            if ($akomodasi->chse == 'Memiliki' OR $akomodasi->chse == 'Ya') {$chse = 1;} else {$chse = 0;}
            if ($akomodasi->peduli_lindungi == 'Memiliki' OR $akomodasi->peduli_lindungi == 'Ya') {$peduli = 1;} else {$peduli = 0;}
            $akomodasiEkrafINS = new EkrafAkomodasi;
            $akomodasiEkrafINS->akomodasi_nama = $akomodasi->nama;
            $akomodasiEkrafINS->akomodasi_kamar = $akomodasi->kamar;
            $akomodasiEkrafINS->akomodasi_tdup = $tdup;
            $akomodasiEkrafINS->akomodasi_chse = $chse;
            $akomodasiEkrafINS->akomodasi_vaksin = $peduli;
            $akomodasiEkrafINS->akomodasi_whatsapp = $akomodasi->whatsapp;
            $akomodasiEkrafINS->generated_at = $akomodasi->generated_at;
            $akomodasiEkrafINS->migrate = 1;
            $akomodasiEkrafINS->users_id = Auth::user()->id;
            $akomodasiEkrafINS->save();
            $kunjunagan = KunjunganAkomodasi::where('akomodasi_id', $akomodasi->id)->get();
            $coll = collect($kunjunagan);
            foreach ($coll as $coll) {
                # code...
                $data =[
                    ['bulan' => $coll->bulan, 'jumlah' => $coll->jumlah, 'akomodasi_id'=> $akomodasiEkrafINS->id, 'users_id'=> Auth::user()->id ],
                   ];
                   EkrafAkomodasiKunjungan::insert($data);
            }
            //UPDATE AKOMODASI SDK
            $akomodasi->migrate = 1;
            $akomodasi->save();
            toast('Migration Success', 'success');
            return redirect()->back();
        }
       


        
    }
}
