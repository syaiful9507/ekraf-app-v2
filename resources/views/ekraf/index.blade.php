@extends('layouts.app')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Ekonomi Kreatif</h1>
    </div>
    
    <div class="section-body">
      <h2 class="section-title">Data Ekraf</h2>

      <div class="row">
        <div class="col-12">
          <div class="card">
            @if (Auth::user()->level === 1 OR Auth::user()->level === 0)
            <div class="card-header">
              <a href="{{ route('ekraf.create') }}"><button type="button" class="btn btn-warning">Tambah</button></a>
              <a href="{{ route('ekraf.export', $code) }}" target="_blank"><button type="button" class="btn btn-primary">EXPORT EXCEL</button></a>
            </div>
            @endif
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>                                 
                    <tr>
                      <th class="text-center">
                        #
                      </th>
                      <th>Nama</th>
                      <th>Alamat</th>
                      <th>Kecamatan</th>
                      <th>WhatsApp</th>
                      <th>Kepemilikan</th>
                      <th>Sub Sektor</th>
                      <th>Haki</th>
                      <th>Tahun berdiri</th>
                      @if ($code != 1)
                      <th>Kurasi</th>
                      @elseif ($code == 1)
                      <th>Data kurasi</th>
                      @endif
                     
                      <th>Chat</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead> 
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($datas as $item)
                    <tr>
                      <td class="text-center">
                        {{ $no++ }}
                      </td>
                      <td>{{ $item->ekraf_nama }}</td>
                      <td>{{ $item->ekraf_alamat }}</td>
                      <td>{{ $item->ekraf_kecamatan }}</td>
                      <td>{{ $item->ekraf_whatsapp }}</td>
                      <td>{{ $item->ekraf_kepemilikan }}</td>
                      <td>{{ $item->ekraf_sub_sektor }}</td>
                      @if ($item->ekraf_haki == 1)
                      <td> Memiliki</td>
                      @else
                      <td> Tidak Memiliki</td>
                      @endif
                      <td>{{ $item->ekraf_tahun_berdiri }}</td>
                      
                      @if ($code != 1)
                      <td>
                        <a href="{{ route('kurasi', [$item->id, $code]) }}" onclick="return confirm('Anda yakin?')"><button type="button" class="btn btn-info" >Kurasi</button></a>
                        </a>
                      </td>
                      @elseif ($code == 1)
                      <td>
                        <a href="{{ route('kurasi', [$item->id, $code]) }}" onclick="return confirm('Anda yakin restore data kurasi?')"><button type="button" class="btn btn-info" >Restore Data</button></a>
                        </a>
                      </td>  
                      @endif
                      
                      <td>
                        <a href="https://api.whatsapp.com/send?phone=62{{ $item->ekraf_whatsapp  }}" target="_blank"><button type="button" class="btn btn-icon icon-left btn-success"><i class="fab fa-whatsapp"></i>Chat WhatsApp</button></a>
                      </td>
                      <td>
                        <a href="{{ route('ekraf.edit', $item->id) }}"><button type="button" class="btn btn-warning">edit</button></a>
                        </a>
                      </td>
                      <td>
                        <a href="{{ route('ekraf.show', $item->id) }}" class="btn btn-primary">Detail</a>
                      </td>
                      
                    </tr>
                    @endforeach                
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>


@endsection