<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rhu extends Model
{
    protected $table = "rhu";
    protected $fillable = ['nama', 'generated_at','tdup', 'whatsapp', 'chse', 'peduli_lindungi','jenis' ];

    public function kunjungan()
    {
        return $this->hasMany('App\KunjunganRhu', 'rhu_id', 'id');
    }

    public static function rhu()
    {
        return Rhu::get()->count();
    }
}
