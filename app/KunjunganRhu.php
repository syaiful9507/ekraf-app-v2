<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KunjunganRhu extends Model
{
    protected $table = "kunjungan_rhu";
    protected $fillable = ['bulan', 'jumlah','jumlah_asing', 'users_id', 'rhu_id'];

    public function ow()
    {
        return $this->belongsTo('App\Rhu','rhu_id', 'id');
    }
 
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function thisYear()
    {
        $thisYear = date('2021');
        return DB::table('kunjungan_rhu')->whereYear(DB::raw('bulan'), '=',$thisYear)->sum(DB::raw('jumlah'));
        
    }

    public static function thisMonth()
    {
        $thisMonth = date('m');
        return DB::table('kunjungan_rhu')->whereMonth(DB::raw('bulan'), '=',$thisMonth)->sum(DB::raw('jumlah + jumlah_asing'));
    }
}
