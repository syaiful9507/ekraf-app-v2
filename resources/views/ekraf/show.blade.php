@extends('layouts.app')


@section('content')


<div class="container-fluid mt--7">
    <div class="row">
    </div>
    <div class="row mt-5">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                    <h3 class="mb-0 text-center">SHOW OBJEK EKRAF</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                    
                           
                      <div class="form-group">
                        <label class="form-control-label">Nama</label>
                        <input type="text" class="form-control" value="{{ $ekraf->ekraf_nama }}" readonly>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">Alamat</label>
                        <input type="text" class="form-control" value="{{ $ekraf->ekraf_alamat }}" readonly>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">Sub Sektor</label>
                        <input type="text" class="form-control" value="{{ $ekraf->ekraf_sub_sektor }}" readonly>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">Kecamatan</label>
                        <input type="text" class="form-control" value="{{ $ekraf->ekraf_kecamatan }}" readonly>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">Desa</label>
                        <input type="text" class="form-control" value="{{ $ekraf->ekraf_desa }}" readonly>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">Kepemilikan</label>
                        <input type="text" class="form-control" value="{{ $ekraf->ekraf_kepemilikan }}" readonly>
                      </div>

                      
                      
                      <div class="form-group">
                        <label class="form-control-label" >Whatsapp/No Telepon</label>
                        <input type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control" id="nomor_telepon" name="nomor_telepon" value="{{ $ekraf->ekraf_whatsapp }}" readonly>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label" >Tahun Berdiri</label>
                        <input type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control" id="tahun_berdiri" name="tahun_berdiri" value="{{ $ekraf->ekraf_tahun_berdiri }}" readonly>
                      </div>
                      

                      <div class="form-group">
                        <label class="form-control-label" >Keterangan</label>
                        <input type="text" class="form-control" id="keterangan" name="keterangan" value="{{ $ekraf->ekraf_keterangan }}" readonly>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label" >Keterangan</label>
                        @if ($ekraf->ekraf_haki == 1)
                        <input type="text" class="form-control" value="Memiliki" readonly>
                        @else
                        <input type="text" class="form-control" value="Tidak Memiliki" readonly>
                        @endif
                        
                      </div>

                      

                </div>
                
            </div>
        </div>
    </div>
</div>


@endsection
@push('script')
<script>
  $(function () {

        $('#district').on('change', function () {
        axios.post('{{ route('village') }}', {id: $(this).val()})
            .then(function (response) {
                $('#village').empty();
                $.each(response.data, function (id, name) {
                    $('#village').append(new Option(name, id))
                })
            });        
    });
  });
</script>
@endpush
