<?php

namespace App\Exports;

use App\KunjunganOw;
use App\Ow;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class KunjunganOwExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $tahun;
    function __construct($tahun) {
            $this->tahun = $tahun;
    }

    
   public function view(): View
   {
    return view('ow.excel.index', [
        'datas' => Ow::with(['kunjungan' => function($query) {
            $query->whereYear('bulan',$this->tahun);
        }])->get()
    ]);
   }
}
