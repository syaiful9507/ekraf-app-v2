@extends('layouts.app')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Objek Wisata</h1>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
              <i class="far fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Objek</h4>
              </div>
              <div class="card-body">
                {{ \App\Ow::ow() }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
              <i class="far fa-newspaper"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Tahun ini</h4>
              </div>
              <div class="card-body">
                {{ \App\KunjunganOw::thisYear() }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
              <i class="far fa-file"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Bulan ini</h4>
              </div>
              <div class="card-body">
                {{ \App\KunjunganOw::thisMonth() }}
              </div>
            </div>
          </div>
        </div>
                       
    </div>

    <div class="row">
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>Statistik Line Chart</h4>
          </div>
          <div class="card-body">
            <canvas id="lineChart" >
            </canvas>
          </div>
        </div>
      </div>
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>Statistik Bar Chart</h4>
          </div>
          <div class="card-body">
            <canvas id="bar-Chart" >
            </canvas>
          </div>
        </div>
      </div>
    </div>
    <div class="section-body">
      <h2 class="section-title">Data Objek</h2>

      <div class="row">
        <div class="col-12">
          <div class="card">
            @if (Auth::user()->level === 1 OR Auth::user()->level === 0)
            <div class="card-header">
              <button type="button" class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="far fa-edit"></i>Masukkan Objek</button>
              
              <div class="dropdown d-inline mr-2">
                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Export Excel
                </button>
                <div class="dropdown-menu">
                  @foreach ($kunjunganTahun as $tahun)
                  <a class="dropdown-item" href="{{ route('exportow', $tahun->year ) }}">{{ $tahun->year }}</a>
                  @endforeach
                  
                </div>
              </div>

              <div class="dropdown d-inline mr-2">
                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Export Asing Saja
                </button>
                <div class="dropdown-menu">
                  @foreach ($kunjunganTahun as $tahun)
                  <a class="dropdown-item" href="{{ route('exportowasing', $tahun->year ) }}">{{ $tahun->year }}</a>
                  @endforeach
                  
                </div>
              </div>


          </div>
            @endif
            

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>                                 
                    <tr>
                      <th class="text-center">
                        #
                      </th>
                      <th>Nama Objek</th>
                      <th>Migrate</th>
                      <th>WhatsApp</th>
                      <th>TDUP</th>
                      <th>CHSE</th>
                      <th>Peduli Lindungi</th>
                      <th>Jenis Objek</th>
                      <th>Kunjungan</th>
                      <th>Chat</th>
                      <th>Action</th>
                    </tr>
                  </thead> 
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($datas as $item)
                    <tr>
                      <td class="text-center">
                        {{ $no++ }}
                      </td>
                      <td>{{ $item->nama }}</td>
                      <td><a href="{{ route('ow.migration', $item->id) }}" class="btn btn-primary" data-nama="{{ $item->nama }}" data-id="{{ $item->id }}" data-toggle="modal" data-target="#migrate">Migrate</a></td>
                      <td>{{ $item->whatsapp }}</td>
                      <td>{{ $item->tdup }}</td>
                      <td>{{ $item->chse }}</td>
                      <td>{{ $item->peduli_lindungi }}</td>
                      <td>{{ $item->jenis }}</td>
                      <td>
                        @if (Auth::user()->level === 1 OR Auth::user()->level === 0)
                          @if (( $item->generated_at == date('2021')))
                          <div class="badge badge-success">Generated</div>
                          @else
                          <button type="button" class="btn btn-icon icon-left btn-primary" data-id="{{ $item->id }}" data-nama="{{ $item->nama }}" data-toggle="modal" data-target="#tambah"><i class="fas fa-cube"></i>Generate Bulan</button>
                          @endif
                        @else
                          @if (( $item->generated_at == date('2021')))
                          <div class="badge badge-success">Generated</div>
                          @else
                          <div class="badge badge-warning">Not Generate</div>
                          @endif
                        @endif
                          
                      </td>
                      <td>
                        <a href="https://api.whatsapp.com/send?phone=62{{ $item->whatsapp }}" target="_blank"><button type="button" class="btn btn-icon icon-left btn-success"><i class="fab fa-whatsapp"></i>Chat WhatsApp</button></a>
                      </td>
                      <td>
                        @if (Auth::user()->level === 1 OR Auth::user()->level === 0)
                        <button type="button" class="btn btn-icon icon-left btn-danger" data-id="{{ $item->id }}" data-nama="{{ $item->nama }}" data-whatsapp="{{ $item->whatsapp }}" data-chse="{{ $item->chse }}" data-peduli="{{ $item->peduli_lindungi }}" data-tdup="{{ $item->tdup }}" data-jenis="{{ $item->jenis }}" data-toggle="modal" data-target="#edit"><i class="far fa-edit"></i>Edit</button>
                            
                        @endif
                        <a href="{{ route('k_ow.show', $item->id) }}" class="btn btn-primary">Detail</a>
                      </td>
                      
                    </tr>
                    @endforeach                
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<div class="modal fade" id="migrate" tabindex="-1" role="dialog" aria-labelledby="migrateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     
      <div class="modal-body">
        <form method="POST" action="{{ route('ow.migration') }}" enctype="multipart/form-data" id="myform">
          @csrf
          <div class="card">
            <div class="card-header">
              <h4>Migrasi Data</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputAddress">Objek Wisata</label>
                <input type="text" class="form-control" id="nama" name="nama" readonly>
                <input type="hidden" name="CODE" value="EXIST">
                <input type="hidden" name="id" id="id">
              </div>
              <div class="form-group">
                <label>Pilih data yang sesuai</label>
                <select class="form-control"  data-toggle="select" name="idOwEkraf" required>
                  @foreach ($ekrafow as $item)
                  <option value="{{ $item->id }}">{{ $item->ow_nama }}</option>
                  @endforeach
                </select>
              </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-danger btn-lg btn-block">Migrate</button>
            </div>
              
            </div>
          </div>
        </form>

        <form method="POST" action="{{ route('ow.migration') }}" enctype="multipart/form-data" id="myform">
          @csrf
          <div class="card">
            <div class="card-footer">
              <h4>Data tidak ada ? klik dibawah</h4>
              <input type="hidden" name="CODE" value="NEW">
              <input type="hidden" id="id" name="id">

            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-warning btn-lg btn-block">Migrate New Data</button>
            </div>
          </div>
        </form>

      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     
      <div class="modal-body">
        <form method="POST" action="{{ route('ow.store') }}" enctype="multipart/form-data">
          @csrf
          <div class="card">
            <div class="card-header">
              <h4>Input Data Objek</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputAddress">Nama Objek</label>
                <input type="text" class="form-control"  placeholder="Masukkan nama " name="nama" required>
              </div> 
              <div class="form-group">
                <label for="inputAddress">WhatsApp</label>
                <input type="number" class="form-control"  placeholder="Masukkan Whatsapp " name="whatsapp" required>
              </div>
              <div class="form-group">
                <label>TDUP</label>
                <select class="custom-select" name="tdup" required>
                  <option selected>Pilih</option>
                  <option value="Tidak Memiliki">Tidak Memiliki</option>
                  <option value="Memiliki">Memiliki</option>
                </select>
              </div>

              <div class="form-group">
                <label>CHSE</label>
                <select class="custom-select" name="chse" required>
                  <option selected>Pilih</option>
                  <option value="Tidak Memiliki">Tidak Memiliki</option>
                  <option value="Memiliki">Memiliki</option>
                </select>
              </div>
              <div class="form-group">
                <label>Peduli Lindungi</label>
                <select class="custom-select" name="peduli" required>
                  <option selected>Pilih</option>
                  <option value="Tidak Memiliki">Tidak Memiliki</option>
                  <option value="Memiliki">Memiliki</option>
                </select>
              </div>
              <div class="form-group">
                <label>Jenis Objek</label>
                <select class="custom-select" name="jenis" required>
                  <option selected>Pilih</option>
                  <option value="Alam">Alam</option>
                  <option value="Buatan">Buatan</option>
                  <option value="Budaya">Budaya</option>
                </select>
              </div>
            </div>
            <div class="card-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     
      <div class="modal-body">
        <form method="POST" action="{{ route('ow.update', 0 ) }}" enctype="multipart/form-data">
          @method('PUT')
          @csrf
          <div class="card">
            <div class="card-header">
              <h4>Edit Data Objek</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputAddress">Nama Objek</label>
                <input type="text" class="form-control"  placeholder="Masukkan nama " id="nama" name="nama" required>
                <input type="hidden" class="form-control"  id="id" name="id" required>
              </div>
              
              <div class="form-group">
                <label for="inputAddress">WhatsApp</label>
                <input type="text" class="form-control" id="whatsapp" name="whatsapp" required>
                <input type="hidden" class="form-control"  id="id" name="id" required>
              </div>
              <div class="form-group">
                <label for="inputAddress">TDUP</label>
                <input type="text" class="form-control"  placeholder="Memiliki/Tidak " id="tdup" name="tdup" required>
              </div>
              <div class="form-group">
                <label for="inputAddress">CHSE</label>
                <input type="text" class="form-control"   id="chse" name="chse" required>
              </div>
              <div class="form-group">
                <label for="inputAddress">Peduli Lindungi</label>
                <input type="text" class="form-control"   id="peduli" name="peduli" required>
              </div>
              <div class="form-group">
                <label for="inputAddress">Jenis Objek</label>
                <input type="text" class="form-control"   id="jenis" name="jenis" readonly>
              </div>

              <div class="form-group">
                <label>Ubah Jenis Objek</label>
                <select class="custom-select" name="jenis" required>
                  <option selected>Pilih</option>
                  <option value="Alam">Alam</option>
                  <option value="Buatan">Buatan</option>
                  <option value="Budaya">Budaya</option>
                </select>
              </div>
            </div>
            

            <div class="card-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambahLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     
      <div class="modal-body">
        <form method="POST" action="{{ route('k_ow.store') }}" enctype="multipart/form-data" id="myform">
          @csrf
          <div class="card"> 
            <div class="card-header">
              <h4>Tambah Kunjungan </h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <input type="text" class="form-control"  id="nama" readonly>
                <p>Silahkan klik Generate untuk membuat pencatatan kunjungan</p>
                <input type="hidden" class="form-control"  id="id" name="id" required>
                <!--
                <div class="form-group">
                  <label>Bulan</label>
                  <input type="month" class="form-control" name="bulan" required>
                </div>
                <div class="form-group">
                  <label>Jumlah Domestik</label>
                  <input type="number" class="form-control" name="jumlah" id="jumlah" required>
                </div>
                <div class="form-group">
                  <label>Jumlah Asing</label>
                  <input type="number" class="form-control" name="jumlah_asing" id="jumlah_asing" required>
                </div>
                -->
              </div>
            </div>

            <div class="card-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Generate</button>
            </div>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

@section('script')
<script>
  $('#edit').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var nama = button.data('nama') // Extract info from data-* attributes
  var id   = button.data('id')
  var whatsapp   = button.data('whatsapp')
  var tdup = button.data('tdup')
  var chse = button.data('chse')
  var peduli = button.data('peduli')
  var jenis = button.data('jenis')
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-body #nama').val(nama);
  modal.find('.modal-body #id').val(id);
  modal.find('.modal-body #whatsapp').val(whatsapp);
  modal.find('.modal-body #tdup').val(tdup);
  modal.find('.modal-body #chse').val(chse);
  modal.find('.modal-body #peduli').val(peduli);
  modal.find('.modal-body #jenis').val(jenis);


})

$('#tambah').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id   = button.data('id')
  var nama   = button.data('nama')
  
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-body #id').val(id);
  modal.find('.modal-body #nama').val(nama);

}),
$('#migrate').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var id   = button.data('id')
  var nama   = button.data('nama')
  var modal = $(this)
  modal.find('.modal-body #id').val(id);
  modal.find('.modal-body #nama').val(nama);
  

})

$(function(){
      //get the pie chart canvas
      var cData = JSON.parse('<?php echo $chart_data; ?>');
      var ctx = $("#line-chart");
 
      //pie chart data
      var data = {
        labels: cData.label,
        datasets: [
          {
            label: "Kunjungan",
            data: cData.data,
            borderWidth: 2,
            backgroundColor: '#6777ef',
            borderColor: '#6777ef',
            borderWidth: 2.5,
            pointBackgroundColor: '#ffffff',
            pointRadius: 4,
            fill: false
          }
        ]
      };
 
      //options
      var options = {
        responsive: true,
        title: {
          display: true,
          position: "top",
          text: "Grafik Kunjungan Perbulan Tahun Ini",
          fontSize: 18,
          fontColor: "#111"
        },
        legend: {
          display: false
          
        }
      };
 
      //create Pie Chart class object
      var chart1 = new Chart(ctx, {
        type: "line",
        data: data,
        options: options
      });
 
  });

  


var cData = JSON.parse('<?php echo $chart_data; ?>');
var dCanvas = document.getElementById("lineChart");


var Domestik = {
  label: 'Domestik',
  data: cData.data1,
  backgroundColor: 'rgba(0, 99, 132, 0.6)',
  borderWidth: 0,
};

var Inter = {
  label: 'Asing',
  data: cData.data2,
  backgroundColor: 'rgba(99, 132, 0, 0.6)',
  borderWidth: 1,
};

var planetData = {
  labels: cData.label,
  datasets: [Domestik, Inter]
};

var chartOptions = {
  scales: {
    xAxes: [{
      barPercentage: 1,
      categoryPercentage: 0.6
    }],
    
  }
};

var barChart = new Chart(dCanvas, {
  type: 'line',
  data: planetData,
  options: chartOptions
});

var cData = JSON.parse('<?php echo $chart_data; ?>');
var dCanvas = document.getElementById("bar-Chart");


var Domestik = {
  label: 'Domestik',
  data: cData.data1,
  backgroundColor: 'rgba(0, 99, 132, 0.6)',
  borderWidth: 0,
};

var Inter = {
  label: 'Asing',
  data: cData.data2,
  backgroundColor: 'rgba(99, 132, 0, 0.6)',
  borderWidth: 1,
};

var planetData = {
  labels: cData.label,
  datasets: [Domestik, Inter]
};

var chartOptions = {
  scales: {
    xAxes: [{
      barPercentage: 1,
      categoryPercentage: 0.6
    }],
    
  }
};

var barChart = new Chart(dCanvas, {
  type: 'bar',
  data: planetData,
  options: chartOptions
});

  

jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});
$( "#myform" ).validate({
  rules: {
    jumlah: {
      required: true,
      number: true
    }
  }
});



</script>
@endsection

@endsection