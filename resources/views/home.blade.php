@extends('layouts.app')
@section('content')
<section class="section">
    <div class="section-header">
      <h1>Dashboard Ekonomi Kreatif</h1>
    </div>
    <div class="row">

      @foreach ($datas as $item)
      <div class="col-lg-4 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-primary">
            <i class="far fa-user"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>{{$item->ekraf_sub_sektor  }}</h4>
            </div>
            <div class="card-body">
              {{ $item->total }}
            </div>
          </div>
        </div>
      </div>
      
      @endforeach  
    </div>

  </section>

@endsection
