@extends('layouts.app')

@section('content')
<div class="container-fluid mt-7">
    <div class="row">
    </div>
    <div class="row mt-5">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                
                <form method="post" action="{{ route('ekraf.store') }}" >
                    @csrf
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                    <h3 class="mb-0 text-center">TAMBAH EKRAF</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                    
                           
                      <div class="form-group">
                        <label class="form-control-label">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama" required>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label" >Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan Alamat" required>
                      </div>
                      
                      <div class="form-group">
                        <label class="form-control-label">Kecamatan</label>
                        <select name="district" id="district" class="form-control">
                            <option value="">== Pilih Kecamatan ==</option>
                            @foreach ($district as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label">Desa</label>
                        <select name="village" id="village" class="form-control">
                            <option value="">== Pilih Desa ==</option>
                          </select>
                      </div>
                      
                      <div class="form-group">
                        <label class="form-control-label">Kepemilikan</label>
                        <select name="kepemilikan" id="kepemilikan" class="form-control">
                            <option value="">== Pilih Kepemilikan ==</option>
                            @foreach ($ownerships as  $name)
                                <option value="{{ $name }}">{{ $name }}</option>
                            @endforeach
                          </select>
                      </div> 
                      
                      <div class="form-group">
                        <label class="form-control-label" >Whatsapp/No Telepon</label>
                        <input type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control" id="nomor_telepon" name="nomor_telepon" placeholder="Masukkan Whatsapp" required>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label" >Tahun Berdiri</label>
                        <input type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control" id="tahun_berdiri" name="tahun_berdiri" placeholder="Masukkan Tahun berdiri">
                      </div>

                      <div class="form-group">
                        <label class="form-control-label" >Sub Sektor</label>
                        <input type="text" class="form-control" id="sub_sektor" name="sub_sektor" placeholder="Masukkan sub sektor">
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">Memiliki Haki</label>
                        <select name="haki" id="haki" class="form-control" required>
                            <option value="">== Pilih Desa ==</option>
                            <option value="0">Tidak</option>
                            <option value="1">Ya</option>
                          </select>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label" >Keterangan</label>
                        <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Masukkan Keterangan">
                      </div>

                </div>
                <div class="modal-footer">
                    <a href="{{ url()->previous() }}" type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>

                
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
  $(function () { 
        $('#district').on('change', function () {
        axios.post('{{ route('village') }}', {id: $(this).val()})
            .then(function (response) {
                $('#village').empty();
                $.each(response.data, function (id, name) {
                    $('#village').append(new Option(name, id))
                })
            });        
    });
  });
</script>
@endpush