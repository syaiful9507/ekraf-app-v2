<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkrafOwKunjungan extends Model
{
    protected $connection = 'mysql2';
    protected $table = "ow_kunjungan";
    protected $fillable = ['bulan', 'jumlah', 'users_id', 'ow_id'];
}
