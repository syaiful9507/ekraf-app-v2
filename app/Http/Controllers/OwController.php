<?php

namespace App\Http\Controllers;

use App\EkrafAkomodasi;
use App\EkrafOw;
use App\EkrafOwKunjungan;
use App\KunjunganOw;
use App\Ow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class OwController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response 
     */
    public function index()
    {
        $curyear = date('2021'); 
        //$curyear = date('Y'); 
        $datas = Ow::where('migrate', 0)->get();
        $record = KunjunganOw::select([
            DB::raw(DB::raw("DATE_FORMAT(bulan, '%M') as bulan")),
            DB::raw(DB::raw('SUM(jumlah) as jumlah')),
            DB::raw(DB::raw('SUM(jumlah_asing) as asing'))
        ])->where(DB::raw("DATE_FORMAT(bulan, '%Y')" ), $curyear)->groupBy('bulan')->get();

        $data = [];
        foreach ($record as $row) {
            # code...
            $data['label'][] = $row->bulan;
            $data['data1'][] = (int) $row->jumlah;
            $data['data2'][] = (int) $row->asing;
        }
        $data['chart_data'] = json_encode($data);
        
        $kunjunganTahun = KunjunganOw::selectRaw('year(bulan) year, count(*) data')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();
        $ekrafow = EkrafOw::where('migrate', 0)->get();

        return view('ow.index')->with($data)->with('datas', $datas)->with('kunjunganTahun', $kunjunganTahun)->with('ekrafow', $ekrafow);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ow = new Ow();
        $ow->nama = $request->get('nama');
        $ow->tdup = $request->get('tdup');
        $ow->chse = $request->get('chse');
        $ow->peduli_lindungi = $request->get('peduli');
        $ow->whatsapp = $request->get('whatsapp'); 
        $ow->jenis = $request->get('jenis'); 
        $ow->save();
        Alert::success('Sukses', 'Berhasil Ditambah');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->get('id');
        $ow = Ow::find($id);
        $ow->nama = $request->get('nama');
        $ow->whatsapp = $request->get('whatsapp');
        $ow->tdup = $request->get('tdup');
        $ow->chse = $request->get('chse');
        $ow->peduli_lindungi = $request->get('peduli');
        if ($request->get('jenis') != 'Pilih') {
        $ow->jenis = $request->get('jenis');
        }
         
        $ow->save();
        Alert::success('Sukses', 'Berhasil di Update');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function migration(Request $request)
    {

        $code = $request->CODE;
        $id = $request->id;
        $idowEkraf = $request->idOwEkraf;
        if ($code === 'EXIST') {
            
            //DATA UDAH ADA 
            $ow = Ow::find($id);
            if ($ow->tdup == 'Memiliki' OR $ow->tdup == 'Ya') {$tdup = 1;} else {$tdup = 0;}
            if ($ow->chse == 'Memiliki' OR $ow->chse == 'Ya') {$chse = 1;} else {$chse = 0;}
            if ($ow->peduli_lindungi == 'Memiliki' OR $ow->peduli_lindungi == 'Ya') {$peduli = 1;} else {$peduli = 0;}
        
            $owEkrafINS = EkrafOw::find($idowEkraf);
            $owEkrafINS->ow_tdup = $tdup;
            $owEkrafINS->ow_chse = $chse;
            $owEkrafINS->ow_vaksin = $peduli;
            $owEkrafINS->ow_whatsapp = $ow->whatsapp;
            $owEkrafINS->generated_at = $ow->generated_at;
            $owEkrafINS->migrate = 1;
            $owEkrafINS->users_id = Auth::user()->id;
            $owEkrafINS->save();

            $kunjunagan = KunjunganOw::where('objek_wisata_id', $id)->get();
            $coll = collect($kunjunagan);
            foreach ($coll as $coll) {
                # code...
                $data =[
                    ['bulan' => $coll->bulan, 'jumlah' => $coll->jumlah,'jumlah_asing' =>$coll->jumlah_asing, 'ow_id'=> $idowEkraf, 'users_id'=> Auth::user()->id ],
                   ];
                   EkrafOwKunjungan::insert($data);
            }
            
            //UPDATE ow SDK
            $ow->migrate = 1;
            $ow->save();
            toast('Migration Success', 'success');
            return redirect()->back();

        } else {
            //DATA BARU
            $ow = Ow::find($id);
            if ($ow->tdup == 'Memiliki' OR $ow->tdup == 'Ya' ) {$tdup = 1;} else {$tdup = 0;}
            if ($ow->chse == 'Memiliki' OR $ow->chse == 'Ya') {$chse = 1;} else {$chse = 0;}
            if ($ow->peduli_lindungi == 'Memiliki' OR $ow->peduli_lindungi == 'Ya') {$peduli = 1;} else {$peduli = 0;}

            $owEkrafINS = new EkrafOw();
            $owEkrafINS->ow_nama = $ow->nama;
            $owEkrafINS->ow_tdup = $tdup;
            $owEkrafINS->ow_chse = $chse;
            $owEkrafINS->ow_vaksin = $peduli;
            $owEkrafINS->ow_whatsapp = $ow->whatsapp;
            $owEkrafINS->generated_at = $ow->generated_at;
            $owEkrafINS->migrate = 1;
            $owEkrafINS->users_id = Auth::user()->id;
            $owEkrafINS->save();
            $kunjunagan = KunjunganOw::where('objek_wisata_id', $ow->id)->get();
            $coll = collect($kunjunagan);
            foreach ($coll as $coll) {
                # code...
                $data =[
                    ['bulan' => $coll->bulan, 'jumlah' => $coll->jumlah,'jumlah_asing' =>$coll->jumlah_asing, 'ow_id'=> $owEkrafINS->id, 'users_id'=> Auth::user()->id ],
                   ];
                   EkrafOwKunjungan::insert($data);
            }
            //UPDATE ow SDK
            $ow->migrate = 1;
            $ow->save();
            toast('Migration Success', 'success');
            return redirect()->back();
        }
        
    }
}
