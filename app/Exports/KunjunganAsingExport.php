<?php

namespace App\Exports;

use App\Ow;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class KunjunganAsingExport implements FromView
{
    
    protected $tahun;
    function __construct($tahun) {
            $this->tahun = $tahun;
    }

    
   public function view(): View
   {
    return view('ow.excel.asing', [
        'datas' => Ow::with(['kunjungan' => function($query) {
            $query->whereYear('bulan',$this->tahun);
        }])->get()
    ]);
   }
}
