<?php

namespace App\Http\Controllers;

use App\AkomodasiEkraf;
use App\EkrafEkraf;
use App\KunjunganAkomodasi;
use App\KunjunganOw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;
use Laravolt\Indonesia\Models\Village;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        //$ekraf = AkomodasiEkraf::all();
        //$curyear = date('Y'); 
        $curyear = date('2021'); 
        $record1 = KunjunganAkomodasi::select([
            DB::raw(DB::raw("DATE_FORMAT(bulan, '%M') as bulan")),
            DB::raw(DB::raw('SUM(jumlah) as jumlah'))
        ])->where(DB::raw("DATE_FORMAT(bulan, '%Y')" ), $curyear)->groupBy('bulan')->get();

        $data1 = [];
        foreach ($record1 as $row) {
            # code...
            $data1['label'][] = $row->bulan;
            $data1['data'][] = (int) $row->jumlah;
        }
        $data1['chart_data1'] = json_encode($data1);

        $record2 = KunjunganOw::select([
            DB::raw(DB::raw("DATE_FORMAT(bulan, '%M') as bulan")),
            DB::raw(DB::raw('SUM(jumlah) as jumlah')),
            DB::raw(DB::raw('SUM(jumlah_asing) as asing'))
        ])->where(DB::raw("DATE_FORMAT(bulan, '%Y')" ), $curyear)->groupBy('bulan')->get();

        $data2 = [];
        foreach ($record2 as $row) {
            # code...
            $data2['label'][] = $row->bulan;
            $data2['data1'][] = (int) $row->jumlah;
            $data2['data2'][] = (int) $row->asing;
        }
        $data2['chart_data2'] = json_encode($data2);

        $dt = EkrafEkraf::get();
        //dd($dt);
        
        $datas = EkrafEkraf::select('ekraf_sub_sektor', DB::raw('count(*) as total'))->groupBy('ekraf_sub_sektor')->get();
        //dd($datak);
        return view('home')->with($data1)->with($data2)->with('datas', $datas);
    }

    public function city(Request $request)
    {
        $cities = City::where('province_id', $request->get('id'))->pluck('name','id');
        return response()->json($cities);
    }
    public function district(Request $request)
    {
        $district = District::where('city_id', $request->get('id'))->pluck('name', 'id');
        return response()->json($district);
    }

    public function village(Request $request)
    {
        $village = Village::where('district_id', $request->get('id'))->pluck('name', 'id');
        return response()->json($village);
    }
}
