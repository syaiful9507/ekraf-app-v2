<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ownership extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'ownerships';
    protected $fillable = ['name'];
}
