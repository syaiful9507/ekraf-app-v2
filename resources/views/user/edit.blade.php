@extends('layouts.app')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>User Akses</h1>
    </div>
    
    @error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror    
    <div class="section-body">
      <h2 class="section-title">Data User</h2>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('user.update', Auth::user()->id ) }}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="card">
                      <div class="card-header">
                        <h4>Data User</h4>
                      </div>
                      <div class="card-body">
                          <div class="card-body">
                              <div class="form-group">
                                <label >Nama</label>
                                <input type="text" class="form-control"  placeholder="Masukkan nama " id="name" name="name" value="{{ $user->name }}" required>
                                
                              </div>
                              <div class="form-group">
                                <label >Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" readonly>
                                
                              </div>
                              <div class="form-group">
                                <label >Password</label>
                                <input type="password" class="form-control"  placeholder="Masukkan Password" name="password" required>
                               
                              </div>
                            </div>
                      </div>
          
                      <div class="card-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                      </div>
                    </div>
                  </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>







@endsection