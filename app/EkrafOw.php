<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkrafOw extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'ow';
    protected $fillable = ['ow_nama','ow_alamat', 'ow_kecamatan', 
    'ow_desa','ow_kepemilikan','ow_whatsapp','ow_tahun_berdiri',
    'ow_manajemen','ow_chse','ow_tdup','ow_vaksin','ow_keterangan','generated_at','users_id'];
}
