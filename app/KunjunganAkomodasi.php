<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KunjunganAkomodasi extends Model
{
    protected $table = "kunjungan_akomodasi";
    protected $fillable = ['bulan', 'jumlah', 'users_id', 'akomodasi_id'];

    public function akomodasi()
    {
        return $this->belongsTo('App\Akomodasi');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public static function thisYear()
    {
        $thisYear = date('2021');
        return DB::table('kunjungan_akomodasi')->whereYear(DB::raw('bulan'), '=',$thisYear)->sum(DB::raw('jumlah'));  
    }

    public static function thisMonth()
    {
        $thisMonth = date('m');
        return DB::table('kunjungan_akomodasi')->whereMonth(DB::raw('bulan'), '=',$thisMonth)->sum(DB::raw('jumlah'));
    }
}
