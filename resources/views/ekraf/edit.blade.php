@extends('layouts.app')


@section('content')


<div class="container-fluid mt--7">
    <div class="row">
    </div>
    <div class="row mt-5">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <form method="POST" action="{{ route('ekraf.update', $ekraf->id) }}" enctype="multipart/form-data"  >
                  @csrf
                  @method('PUT')
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                    <h3 class="mb-0 text-center">EDIT OBJEK EKRAF</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                    
                           
                      <div class="form-group">
                        <label class="form-control-label">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ $ekraf->ekraf_nama }}" required>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" value="{{ $ekraf->ekraf_alamat }}" required>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">Sub Sektor</label>
                        <input type="text" class="form-control" id="sub_sektor" name="sub_sektor" value="{{ $ekraf->ekraf_sub_sektor }}" required>
                      </div>

                      
                      <div class="form-group">
                        <label class="form-control-label">Kecamatan</label>
                        <input type="text" class="form-control" value="{{ $ekraf->ekraf_kecamatan }}" readonly>
                        <select name="district" id="district" class="form-control">
                            <option value="">== Pilih Kecamatan ==</option>
                            @foreach ($district as $id => $name)
                                <option value="{{ $id }}" {{ $name === $ekraf->ekraf_kecamatan ? 'selected' : '' }}>{{ $name }}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label">Desa</label>
                        <input type="text" class="form-control" value="{{ $ekraf->ekraf_desa }}" readonly>
                        <select name="village" id="village" class="form-control" >
                            <option value="">== Pilih Desa ==</option>
                          </select>
                      </div>
                      
                      <div class="form-group">
                        <label class="form-control-label">Kepemilikan</label>
                        <select name="kepemilikan" id="kepemilikan" class="form-control" required>
                            <option value="">== Pilih Kepemilikan ==</option>
                            @foreach ($ownerships as  $name)
                                <option value="{{ $name }}" {{ $name === $ekraf->ekraf_kepemilikan ? 'selected' : '' }}>{{ $name }}</option>
                            @endforeach
                          </select>
                      </div>
                      
                      <div class="form-group">
                        <label class="form-control-label" >Whatsapp/No Telepon</label>
                        <input type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control" id="nomor_telepon" name="nomor_telepon" value="{{ $ekraf->ekraf_whatsapp }}" required>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label" >Tahun Berdiri</label>
                        <input type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control" id="tahun_berdiri" name="tahun_berdiri" value="{{ $ekraf->ekraf_tahun_berdiri }}" required>
                      </div>
                      

                      <div class="form-group">
                        <label class="form-control-label" >Keterangan</label>
                        <input type="text" class="form-control" id="keterangan" name="keterangan" value="{{ $ekraf->ekraf_keterangan }}">
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">Memiliki HAKI ?</label>
                        <select name="haki" id="haki" class="form-control" required>
                            <option value="">== Pilih ==</option>
                            <option value="0" {{ $ekraf->ekraf_haki == 0 ? 'selected' : '' }}>Tidak</option>
                            <option value="1" {{ $ekraf->ekraf_haki == 1 ? 'selected' : '' }}>Iya</option>
                          </select>
                      </div>

                </div>
                <div class="modal-footer">
                    <a href="{{ url()->previous() }}" type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>

                
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
@push('script')
<script>
  $(function () {

        $('#district').on('change', function () {
        axios.post('{{ route('village') }}', {id: $(this).val()})
            .then(function (response) {
                $('#village').empty();
                $.each(response.data, function (id, name) {
                    $('#village').append(new Option(name, id))
                })
            });        
    });
  });
</script>
@endpush
