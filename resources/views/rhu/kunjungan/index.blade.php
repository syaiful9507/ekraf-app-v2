@extends('layouts.app')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Rekreasi Hiburan Umum</h1>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
              <i class="far fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Rata - Rata perbulan</h4>
              </div>
              <div class="card-body">
                {{ $avgPerMonth }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
              <i class="far fa-newspaper"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Tahun ini</h4>
              </div>
              <div class="card-body">
                {{ $KunThisYear }}
              </div>
            </div>
          </div>
        </div>         
    </div>
    <div class="row">
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>Statistik Line Chart</h4>
          </div>
          <div class="card-body">
            <canvas id="lineChart" >
            </canvas>
          </div>
        </div>
      </div>
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>Statistik Pie Chart</h4>
          </div>
          <div class="card-body">
            <canvas id="bar-Chart" >
            </canvas>
          </div>
        </div>
      </div>
    </div>

    <div class="section-body">
      <h2 class="section-title">Data Kunjungan {{ $rhu->nama }}</h2>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>                                 
                    <tr>
                      <th class="text-center">
                        #
                      </th>
                      <th>Bulan</th>
                      <th>Domestik</th>
                      <th>Asing</th>
                      <th>Tahun</th>
                      @if (Auth::user()->level === 1 OR Auth::user()->level === 0)
                      <th>Action</th>
                      @endif
                    </tr>
                  </thead>
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($datas as $item)
                    <tr>
                      <td class="text-center">
                        {{ $no++ }}
                      </td>
                      <td>{{ \Carbon\Carbon::parse($item->bulan)->format('F')}}</td>
                      <td>{{ $item->jumlah }}</td>
                      <td>{{ $item->jumlah_asing }}</td>
                      <td>{{ \Carbon\Carbon::parse($item->bulan)->format('Y')}}</td>
                      @if (Auth::user()->level === 1 OR Auth::user()->level === 0)
                      <th>
                        <button type="button" class="btn btn-icon icon-left btn-danger" data-asing="{{ $item->jumlah_asing }}" data-id="{{ $item->id }}" data-jumlah="{{ $item->jumlah }}" data-bulan="{{ \Carbon\Carbon::parse($item->bulan)->format('F')}}" data-toggle="modal" data-target="#edit"><i class="far fa-edit"></i>Edit</button>
                      </th>
                      @endif
                      
                      
                    </tr>
                    @endforeach                
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<!--
<div class="modal fade" id="tambahkunjungan" tabindex="-1" role="dialog" aria-labelledby="tambahkunjunganLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
       
        <div class="modal-body">
          <form method="POST" action="{{ route('k_ow.store') }}" enctype="multipart/form-data" id="myform">
            @csrf
            <div class="card">
              <div class="card-header">
                <h4>Tambah Kunjungan </h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" class="form-control"  id="id" name="id" value="{{ $rhu->id }}" required>
                  <div class="form-group">
                    <label>Bulan</label>
                    <input type="month" class="form-control" name="bulan" required>
                  </div>
                  <div class="form-group">
                    <label>Jumlah Domestik</label>
                    <input type="number" class="form-control" name="jumlah" id="jumlah" required>
                  </div>
                  <div class="form-group">
                    <label for="inputAddress">Jumlah Asing</label>
                    <input type="number" class="form-control"  id="jumlah_asing" name="jumlah_asing" required>
                  </div>
                </div>
              </div>
  
              <div class="card-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>
-->

  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
       
        <div class="modal-body">
          <form method="POST" action="{{ route('k_rhu.update', 0 ) }}" enctype="multipart/form-data" id="myForm">
            @method('PUT')
            @csrf
            <div class="card">
              <div class="card-header">
                <h4>Edit kunjungan </h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <div class="form-group">
                    <label for="inputAddress">Bulan</label>
                    <input type="text" class="form-control"  id="bulan" name="bulan" readonly>
                  </div>
                  <div class="form-group">
                    <label for="inputAddress">Jumlah lokasl</label>
                    <input type="number" class="form-control"  id="jumlah" name="jumlah" required>
                  </div>
                  <div class="form-group">
                    <label for="inputAddress">Jumlah Asing</label>
                    <input type="number" class="form-control"  id="jumlah_asing" name="jumlah_asing" required>
                  </div>
                  
                  <input type="hidden" class="form-control"  id="id" name="id" required>
                </div>
              </div>
  
              <div class="card-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>

@section('script')
<script>
  $('#edit').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var bulan = button.data('bulan')
    var jumlah = button.data('jumlah')
    var jumlah_asing = button.data('asing')
    var id   = button.data('id')
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-body #bulan').val(bulan);
    modal.find('.modal-body #jumlah').val(jumlah);
    modal.find('.modal-body #jumlah_asing').val(jumlah_asing);
    modal.find('.modal-body #id').val(id);
    
  })

  $(function(){ 
      //get the pie chart canvas
      var cData = JSON.parse('<?php echo $chart_data; ?>');
      var ctx = $("#line-chart");
 
      //pie chart data
      var data = {
        labels: cData.label,
        datasets: [
          {
            label: "Kunjungan",
            data: cData.data1,
            borderWidth: 2,
            backgroundColor: '#6777ef',
            borderColor: '#6777ef',
            borderWidth: 2.5,
            pointBackgroundColor: '#ffffff',
            pointRadius: 4,
            fill: false
          }
        ]
      };
 
      //options
      var options = {
        responsive: true,
        title: {
          display: true,
          position: "top",
          text: "Grafik Kunjungan Perbulan Tahun Ini",
          fontSize: 18,
          fontColor: "#111"
        },
        legend: {
          display: false
          
        }
      };
 
      //create Pie Chart class object
      var chart1 = new Chart(ctx, {
        type: "line",
        data: data,
        options: options
      });
 
  });

  $(function(){
      //get the pie chart canvas
      var cData = JSON.parse('<?php echo $chart_data; ?>');
      var ctx = $("#pie-chart");
 
      //pie chart data
      var data = {
        labels: cData.label,
        datasets: [
          {
            label: "Kunjungan",
            data: cData.data1,
            borderWidth: 2,
            borderWidth: 2.5,
            pointBackgroundColor: '#ffffff',
            pointRadius: 4,
            backgroundColor: [
              "#E0F2F1",
              "#4DD0E1",
              "#29B6F6",
              "#0091EA",
              "#DCE775",
              "#1D7A46",
              "#CDA776",
              "#F44336",
              "#EF9A9A",
              "#6A1B9A",
              "#26A69A",
              "#E65100",

            ],
            borderColor: [
              "#E0F2F1",
              "#4DD0E1",
              "#29B6F6",
              "#0091EA",
              "#DCE775",
              "#1D7A46",
              "#CDA776",
              "#F44336",
              "#EF9A9A",
              "#6A1B9A",
              "#26A69A",
              "#E65100",
            ],
            borderWidth: [1, 1, 1, 1, 1,1,1]
          }
        ]
      };
 
      //options
      var options = {
        responsive: true,
        title: {
          display: true,
          position: "top",
          text: "Grafik Kunjungan Perbulan Tahun Ini",
          fontSize: 18,
          fontColor: "#111"
        },
        legend: {
          display: true,
          position: "bottom",
          labels: {
            fontColor: "#333",
            fontSize: 10
          }
          
        }
      };
 
      //create Pie Chart class object
      var chart1 = new Chart(ctx, {
        type: "pie",
        data: data,
        options: options
      });
 
  });

  var cData = JSON.parse('<?php echo $chart_data; ?>');
  var dCanvas = document.getElementById("lineChart");


  var Domestik = {
    label: 'Domestik',
    data: cData.data1,
    backgroundColor: 'rgba(0, 99, 132, 0.6)',
    borderWidth: 0,
  };

  var Inter = {
    label: 'Asing',
    data: cData.data2,
    backgroundColor: 'rgba(99, 132, 0, 0.6)',
    borderWidth: 1,
  };

  var planetData = {
    labels: cData.label,
    datasets: [Domestik, Inter]
  };

  var chartOptions = {
    scales: {
      xAxes: [{
        barPercentage: 1,
        categoryPercentage: 0.6
      }],
      
    }
  };

  var barChart = new Chart(dCanvas, {
    type: 'line',
    data: planetData,
    options: chartOptions
  });


  var cData = JSON.parse('<?php echo $chart_data; ?>');
  var dCanvas = document.getElementById("bar-Chart");
  var Domestik = {
    label: 'Domestik',
    data: cData.data1,
    backgroundColor: 'rgba(0, 99, 132, 0.6)',
    borderWidth: 0,
  };

  var Inter = {
    label: 'Asing',
    data: cData.data2,
    backgroundColor: 'rgba(99, 132, 0, 0.6)',
    borderWidth: 1,
  };

  var planetData = {
    labels: cData.label,
    datasets: [Domestik, Inter]
  };

  var chartOptions = {
    scales: {
      xAxes: [{
        barPercentage: 1,
        categoryPercentage: 0.6
      }],
      
    }
  };

  var barChart = new Chart(dCanvas, {
    type: 'bar',
    data: planetData,
    options: chartOptions
  });

</script>

@endsection
@endsection