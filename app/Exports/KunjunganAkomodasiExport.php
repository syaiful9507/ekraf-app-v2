<?php

namespace App\Exports;

use App\Akomodasi;
use Illuminate\Contracts\View\View;
use Illuminate\View\View as ViewView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class KunjunganAkomodasiExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $tahun;
    function __construct($tahun) {
            $this->tahun = $tahun;
    }

    
   public function view(): View
   {
    return view('akomodasi.excel.index', [
        'datas' => Akomodasi::with(['kunjungan' => function($query) {
            $query->whereYear('bulan',$this->tahun);
        }])->get()
    ]);
   }
}
