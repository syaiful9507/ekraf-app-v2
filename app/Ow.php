<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ow extends Model
{
    protected $table = "objek_wisata";
    protected $fillable = ['nama', 'generated_at','tdup', 'whatsapp', 'chse', 'peduli_lindungi','jenis','migrate' ];

    public function kunjungan()
    {
        return $this->hasMany('App\KunjunganOw', 'objek_wisata_id', 'id');
    }

    public static function ow()
    {
        return Ow::get()->count();
    }
}
