
<html>
    <head>
      <title>DATA EKONOMI KREATIF</title>
      
      <style>
      
      table, td, th {
      border: 1px solid black;
    }
    
    table {
      border-collapse: collapse;
      width: 100%;
    }
    
    th {
      height: 50px;
    }
      </style>
    
    </head>
        <body>
        <table align="center">
                    <th colspan="11" rowspan=2 height="20" bgcolor="#0089c4" align="center">
              <b>DATA EKONOMI KREATIF
              <br>
              LAPORAN DATA EKONOMI KREATIF
            </b>
                </th>
        </table>
    <br>
        <table >
          <tr>
            <th width="10"  height="25" align="center" bgcolor="lightblue">No</th>
            <th width="50"  height="25" align="center" bgcolor="lightblue">Nama</th>
            <th width="80"  height="25" align="center" bgcolor="lightblue">Alamat</th>
            <th width="20"  height="25" align="center" bgcolor="lightblue">Kecamatan</th>
            <th width="20"  height="25" align="center" bgcolor="lightblue">Desa</th>
            <th width="20"  height="25" align="center" bgcolor="lightblue">Kepemilikan</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">WhatsApp</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">Tahun Berdiri</th>
            <td width="10"  height="25" align="center" bgcolor="lightblue">Sub Sektor</td>
            <th width="15"  height="25" align="center" bgcolor="lightblue">Haki</th>
            <th width="15"  height="25" align="center" bgcolor="lightblue">Keterangan</th>
            
          </tr>
    
          @php
          $no = 1;
          @endphp
          @foreach ($datas as $item)
          <tr align="center">
            <td align="center" >{{$no++}}</td>
            <td align="center" >{{$item->ekraf_nama}}</td>
            <td align="center">{{ $item->ekraf_alamat }}</td>
            <td align="center">{{ $item->ekraf_kecamatan }}</td>
            <td align="center">{{ $item->ekraf_desa }}</td>
            <td align="center">{{ $item->ekraf_kepemilikan }}</td>
            <td align="center">{{ $item->ekraf_whatsapp }}</td>
            <td align="center">{{ $item->ekraf_tahun_berdiri }}</td>
            <td align="center">{{ $item->ekraf_sub_sektor }}</td>
            <td align="center">@if ($item->ekraf_haki == 1){{ 'Memiliki' }}@else{{ 'Tidak ada' }}@endif</td>
            <td align="center">{{ $item->ekraf_keterangan }}</td>
          </tr>
          @endforeach
    
          
            </table>
        </body>
    </html>